//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
// File Name	: ipmi_if.c
// 
// Title		: IPMI interface
// Revision		: 1.0
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
// Modified by  : Markus Joos (markus.joos@cern.ch)
//
// Description : The IPMI interface initialization and functions
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************
#include "../../drivers/drivers.h"

#include "../inc/ipmi_if.h"
#include "../inc/hpm.h"
#include "../inc/project.h"

#include "../../user/config.h"

//Globals

ipmi_status status_bits;

LocalBuffer readBuffer;
ipmi_msg_t rqs;
unsigned char rsp_data[MAX_BYTES_READ+5], rsp_data_len, rqSAddr, rqSeq, rsSAddr;


void ipmi_init(){
	ipmb_i2c_configuration(get_address(), ipmb_get);
}

void ipmi_check_request(){
    unsigned char i, j;

    if (status_bits.ipmb_rqs == 0)
	    return;

    if (checsum_verify(readBuffer.Buffer, 2, readBuffer.Buffer[2]) != 0) {                                            // first checksum
		status_bits.ipmb_rqs = 0;
        return; //ignore request
	}
	
    if ((checsum_verify(readBuffer.Buffer, readBuffer.Length - 1, readBuffer.Buffer[readBuffer.Length - 1])) != 0) {  // second checksum
		status_bits.ipmb_rqs = 0;
        return; //ignore request
	}
	
    //fill request
    //For a definition of the format of the request see paragraph 2.11.1 of "Intelligent Platform Management Bus Communications Protocol Specification"

    rsSAddr = readBuffer.Buffer[0];
    rqs.netfn = readBuffer.Buffer[1] >> 2;  //Seems we are not interested in the bottom 2 bits of this byte which contain the responder's LUN
    rqSAddr = readBuffer.Buffer[3];         //Address of the requester (i.e. MCH)
    rqSeq = readBuffer.Buffer[4] >> 2;
    rqs.lun = readBuffer.Buffer[4] & 0x03;
    rqs.cmd = readBuffer.Buffer[5];

    if (rqs.netfn & 0x01){
		status_bits.ipmb_rqs = 0;
		return; //JM: No events are sent
	}
	
    if (readBuffer.Length > 7)
    {
        rqs.data_len = readBuffer.Length - 7;
        for (i = 6, j = 0; i < readBuffer.Length - 1; i++, j++)
            rqs.data[j] = readBuffer.Buffer[i];
    }
    else
        rqs.data_len = 0;
		
	ipmi_response_send();
}

//**********************/
void ipmi_response_send()
//**********************/
{
    unsigned char error = IPMI_CC_OK;
    rsp_data_len = 0;
	
    switch (rqs.netfn)
    {
		case IPMI_APP_NETFN:
			if ((rqs.cmd == IPMI_GET_DEVICE_ID_CMD) || (rqs.cmd == IPMI_BROADCAST_GET_DEVICE_ID_CMD))
				rsp_data_len = ipmi_get_device_id();
			else
				error = IPMI_CC_INV_CMD;
			break;

		case IPMI_GROUP_EXTENSION_NETFN + IPMI_PICMG_GRP_EXT:		
			error = hpm_response_send(rqs.cmd, rqs.data, rqs.data_len, rsp_data, &rsp_data_len);
			break;

		default:
			error = IPMI_CC_INV_CMD;
			break;
    }
			
    ipmb_send_rsp(error);
}

//*********************/
unsigned char ipmi_get_device_id()  //Called from ipmi_response_send in this file
//*********************/
{
    unsigned char len = 0;

    rsp_data[len++] = 0x00;                            // Device ID
    rsp_data[len++] = 0x80;                            // Device revision and SDRs
    rsp_data[len++] = MMC_FW_REL_MAJ;                  // Major Firmware revision
    rsp_data[len++] = MMC_FW_REL_MIN;                  // Minor Firmware revision
    rsp_data[len++] = MMC_IPMI_REL;                    // IPMI version 1.5
    rsp_data[len++] = IPMI_MSG_ADD_DEV_SUPP;           // Additional device support(commands and functions)
    rsp_data[len++] = IPMI_MSG_MANU_ID_LSB;            // Manufacturer ID LSB
    rsp_data[len++] = IPMI_MSG_MANU_ID_B2;
    rsp_data[len++] = IPMI_MSG_MANU_ID_MSB;
    rsp_data[len++] = IPMI_MSG_PROD_ID_LSB;            // Product ID LSB
    rsp_data[len++] = IPMI_MSG_PROD_ID_MSB;

    return len;
}


//***********************************/
unsigned char checksum_clc(unsigned char* buf, unsigned char length)
//***********************************/
{
    unsigned char crc = 0x00;

    while (length--)
        crc += buf[length];

    return (0 - crc);
}


//***************************************************/
unsigned char checsum_verify(unsigned char* buf, unsigned char length, unsigned char checksum)
//***************************************************/
{
    return (checksum - checksum_clc(buf, length));
}


//get message from ipmb 
//***************************************************/
void ipmb_get(unsigned char receiveDataLength, unsigned char* receiveData)  //Installed as asynchronous handler in ipmi_init in this file
//***************************************************/
{
    unsigned char i;

    status_bits.connected = 1;

    if (status_bits.ipmb_rqs == 1)
        return;

    for (i = 0; i < receiveDataLength; i++)   // copy the received data to a local buffer
        readBuffer.Buffer[i] = *receiveData++;

    readBuffer.Length = receiveDataLength;
    status_bits.ipmb_rqs = 1;
}

//master write message
//*****************************/
void ipmb_send_rsp(unsigned char cc_error){
//*****************************/

    LocalBuffer writeBuffer;
    unsigned char i = 0, j;
	
    writeBuffer.Buffer[i++] = rqSAddr;
    writeBuffer.Buffer[i++] = ((rqs.netfn + 1) << 2) | (rqs.lun & 0x03);
    writeBuffer.Buffer[i++] = checksum_clc(writeBuffer.Buffer, 2);
    writeBuffer.Buffer[i++] = rsSAddr;
    writeBuffer.Buffer[i++] = (rqSeq << 2);
    writeBuffer.Buffer[i++] = rqs.cmd;
    writeBuffer.Buffer[i++] = cc_error;

    if (rqs.netfn == IPMI_GROUP_EXTENSION_NETFN)
        writeBuffer.Buffer[i++] = IPMI_PICMG_GRP_EXT;

    if (!cc_error && (rsp_data_len > 0))
    {
        for (j = 0; j < rsp_data_len; j++)
            writeBuffer.Buffer[i++] = rsp_data[j];
    }
		
    writeBuffer.Buffer[i] = checksum_clc(writeBuffer.Buffer, i);
    ipmb_send(rqSAddr, i, &writeBuffer.Buffer[1]);
    status_bits.ipmb_rqs = 0;
}


//get IPMB_L address 	
//**************
unsigned char get_address()
//**************
{
	
    unsigned char geog_addr = 0, GAddr1 = 0, GAddr2 = 0, GAddr3 = 0;
	
    // configure GAx as high impedance input
    clear_signal(GA0);
    clear_signal(GA1);
    clear_signal(GA2);

    set_signal_dir(GA_PULLUP, 0x00);			// configure GA pull-up as low output
    clear_signal(GA_PULLUP);					// set pin to low
		
    delay_us(100);								// give Pin a chance to change

    GAddr1 = get_signal(GA0);
    GAddr2 = get_signal(GA1);
    GAddr3 = get_signal(GA2);

    set_signal(GA_PULLUP);     // set pin to high
    delay_us(100);

    //read lines and compare
    if (GAddr1 != get_signal(GA0))
        GAddr1 = UNCONNECTED;
    if (GAddr2 != get_signal(GA1))
        GAddr2 = UNCONNECTED;
    if (GAddr3 != get_signal(GA2))
        GAddr3 = UNCONNECTED;

	//** Check standard ADDR **
	if(GAddr1 == GROUNDED && GAddr2 == GROUNDED && GAddr3 == UNCONNECTED)			geog_addr = 0x72;
	else if(GAddr1 == GROUNDED && GAddr2 == UNCONNECTED && GAddr3 == GROUNDED)		geog_addr = 0x74;
	else if(GAddr1 == GROUNDED && GAddr2 == UNCONNECTED && GAddr3 == UNCONNECTED)	geog_addr = 0x76;
	else if(GAddr1 == UNCONNECTED && GAddr2 == GROUNDED && GAddr3 == GROUNDED)		geog_addr = 0x78;
	else if(GAddr1 == UNCONNECTED && GAddr2 == GROUNDED && GAddr3 == UNCONNECTED)	geog_addr = 0x7A;
	else if(GAddr1 == UNCONNECTED && GAddr2 == UNCONNECTED && GAddr3 == GROUNDED)	geog_addr = 0x7C;
	else if(GAddr1 == UNCONNECTED && GAddr2 == UNCONNECTED && GAddr3 == POWERED)	geog_addr = 0x7E;
	else if(GAddr1 == UNCONNECTED && GAddr2 == POWERED && GAddr3 == UNCONNECTED)	geog_addr = 0x80;
	else if(GAddr1 == UNCONNECTED && GAddr2 == POWERED && GAddr3 == POWERED)		geog_addr = 0x82;
	else if(GAddr1 == POWERED && GAddr2 == UNCONNECTED && GAddr3 == UNCONNECTED)	geog_addr = 0x84;
	else if(GAddr1 == POWERED && GAddr2 == UNCONNECTED && GAddr3 == POWERED)		geog_addr = 0x86;
	else if(GAddr1 == POWERED && GAddr2 == POWERED && GAddr3 == UNCONNECTED)		geog_addr = 0x88;
	
	//** Check custom ADDR **
	#ifdef CUSTOM_ADDR_LIST
		#define ADDR(p_addr, p_gaddr1, p_gaddr2, p_gaddr3)		else if(GAddr1 == p_gaddr1 && GAddr2 == p_gaddr2 && GAddr3 == p_gaddr3)		geog_addr = p_addr;
		CUSTOM_ADDR_LIST
	#endif
	
    return(geog_addr ? geog_addr : 0x70);
}

