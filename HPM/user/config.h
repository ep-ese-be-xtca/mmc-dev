/*
 * config.h
 *
 * Created: 14/04/2015 13:58:12
 *  Author: jumendez
 */ 


#ifndef CONFIG_H_
#define CONFIG_H_

// NOTE: Manufacturer identification is handled by http://www.iana.org/assignments/enterprise-numbers
#define IANA_MANUFACTURER_ID	0x000060	//CERN IANA (3 bytes)
#define PRODUCT_ID				0x1235		//Product ID (2 bytes)


#endif /* CONFIG_H_ */