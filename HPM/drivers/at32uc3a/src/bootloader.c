/*
 * bootloader.c
 *
 * Created: 18/10/2014 23:27:47
 *  Author: jumendez
 */

#include <asf.h>
#include <string.h>

#include "../inc/bootloader.h"
#include "../../drivers.h"

unsigned int FlashAddr;

/****************************************
HPM commands : HIGH LEVEL INTERFACE
*****************************************/
unsigned int image_size = 0, last_block = 0, tmpcnt = 0, bufptr;

unsigned char buf[AVR32_FLASHC_PAGE_SIZE];

unsigned char buf_last_cmd[25], buf_last_cmd_length;

unsigned char test_cc;
int page_tmp;

void flash_init(){		
	FlashAddr = PROGRAM_START_ADDR;	
	bufptr = 0;
	image_size = 0;
	last_block = 0;
	tmpcnt = 0;
	bufptr = 0;

}

unsigned int addr_offset = 0;

unsigned char flash_write(unsigned char *data, unsigned char data_len){	
		
		int i;	
		int block_size = 0;
		int cc = 0;		
		
		block_size = (data_len-2);
		
		if((tmpcnt == 0 && data[1] == 0x00) || data[1] != last_block){				
			image_size += (data_len-2);
			
			for(i=0; i < block_size; i++){
				buf[bufptr++] = data[2+i]; //testwrite[i];
				
				//If we receive a new value and buffer is full
				if(bufptr >= AVR32_FLASHC_PAGE_SIZE){								//Flash page full, write flash page;otherwise receive next frame
					cc = writeflash (FlashAddr, buf, AVR32_FLASHC_PAGE_SIZE);			//write data to Flash
					FlashAddr += AVR32_FLASHC_PAGE_SIZE;							//modify Flash page address
					bufptr = 0;
				}
			}
			
			last_block = data[1];	
			tmpcnt++;
		}else{
			cc = 0x82;
		}
				
		return cc;
}
					
unsigned char flash_check(unsigned int length){
		
	unsigned int i;
	unsigned char completion_code;
	
	//printf("hmp_finish_firmware_upload () \n\r");		
	
	if (bufptr > 0){
		for(i=bufptr; i < AVR32_FLASHC_PAGE_SIZE; i++)
			buf[i] = 0xff;
		if(writeflash (FlashAddr, buf, AVR32_FLASHC_PAGE_SIZE))
			return 0x81;
	}

	if(length != image_size)
		completion_code = 0x81;
	else
		completion_code = 0x00;
	
	return completion_code;
}

//update one Flash page

unsigned char writeflash(unsigned int addr, unsigned char *ptr, unsigned int size){
	
	//unsigned int flash_add = (addr - (addr % AVR32_FLASHC_PAGE_SIZE));
	//U64 page[AVR32_FLASHC_PAGE_SIZE];
	/*
	Union64 flash_dword;
	unsigned i;
	unsigned page_pos;
	
	while (size) {
		// Clear the page buffer in order to prepare data for a flash page write.
		flashc_clear_page_buffer();
		if(!flashc_erase_page(-1, 1)) return 0x81;
		
		// Loop in the page
		for (page_pos=0; page_pos<AVR32_FLASHC_PAGE_SIZE; page_pos+=sizeof(uint64_t) ) {
			// Read the flash double-word buffer
			flash_dword.u64 = *(volatile uint64_t*)flash_add;

			// Update double-word if necessary
			for (i = 0; i < sizeof(uint64_t); i++) {
				if (size && ((unsigned int)flash_add == addr)) {
					// Update page with data source
					flash_dword.u8[i] = *ptr++;
					addr++;
					size--;
				}
				flash_add++;
			}

			// Write the flash double-word buffer to the page buffer.
			*(volatile uint64_t*)((uint32_t)flash_add - sizeof(uint64_t))= flash_dword.u64;
		}

		flashc_write_page(-1);
	}
	*/
	flashc_memcpy((void *)addr, ptr, size, true);
	
	if (memcmp((void *)addr, ptr, size)){
		return 0x81;
	}else
		return 0x00;
}