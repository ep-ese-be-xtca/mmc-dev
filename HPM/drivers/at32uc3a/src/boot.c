#include <asf.h>

#include "../inc/io.h"
#include "../inc/i2c.h"
#include "../inc/bootloader.h"

#include "../../drivers.h"

void boot_uC (void)
{
	/* Insert system clock initialization code here (sysclk_init()). */
	irq_initialize_vectors();
	
	sysclk_init();
	
	sleepmgr_init();
	flash_init();
	init_port();
	i2cInit();
	
	stdio_usb_init();
	stdio_usb_enable();
		
	cpu_irq_enable();
}

void reset_uC(){
	reset_do_soft_reset();
}

void vbus_event(unsigned char b_vbus_high){
	if(b_vbus_high)	udc_attach();
	else			udc_detach();
}
