/*
 * i2c.c
 *
 * Created: 15/10/2015 22:56:34
 *  Author: jumendez
 */ 

#include <asf.h>
#include <stdio.h>

#include "../../drivers.h"
#include "../inc/i2c.h"
#include "../../../application/inc/ipmi_if.h"

/*****************************************
 GLOBAL VARIABLES
******************************************/
	// receive buffer (incoming data)
	static unsigned char I2cReceiveData[MAX_BYTES_READ+15];
	static unsigned char I2cReceiveDataIndex=1;

	// function pointer to i2c receive routine
	// I2cSlaveReceive is called when this processor is addressed as a slave for writing
	static void (*i2cSlaveReceive)(unsigned char receiveDataLength, unsigned char* recieveData);
	static void (*i2cSlaveTransmit)(unsigned char receiveDataLength, unsigned char* recieveData);


	static twi_master_options_t TWI_MASTER_OPTS = {
		.speed = 100000,
		.chip  = 0
	};

	static twis_slave_fct_t TWI_CBS =
	{
		.rx = &i2c_slave_rx,
		.tx = &i2c_slave_tx,
		.stop = &i2c_slave_stop
	};

	static twis_options_t TWI_SLAVE_OPTS =
	{
		.speed = 100000,
		.chip = 0 // Set @ Runtime
	};

unsigned char stat;

/*****************************************
 INIT FUNCTIONS
******************************************/
void i2cInit(){	
	gpio_enable_module_pin(AVR32_TWIMS1_TWCK_0_PIN, AVR32_TWIMS1_TWCK_0_FUNCTION);
	gpio_enable_module_pin(AVR32_TWIMS1_TWD_0_PIN, AVR32_TWIMS1_TWD_0_FUNCTION);
	
	TWI_SLAVE_OPTS.chip = get_address() >> 1;
	TWI_MASTER_OPTS.chip = get_address() >> 1;
	
	AVR32_TWIM1.CR.swrst = 0x1;
	AVR32_TWIM1.CR.stop = 0x1;
	AVR32_TWIM1.CR.men = 0x1;
	
	twi_master_setup(&AVR32_TWIM1, &TWI_MASTER_OPTS);
	stat=twi_slave_setup( &AVR32_TWIS1, &TWI_SLAVE_OPTS, &TWI_CBS);
}

void ipmb_i2c_configuration(unsigned char address, void (*ipmb_callback)(unsigned char receiveDataLength, unsigned char* recieveData)){	
	i2cSlaveReceive = ipmb_callback;
}

void i2cSetSlaveReceiveHandler(void (*i2cSlaveRx_func)(unsigned char receiveDataLength, unsigned char* recieveData)){
	i2cSlaveReceive = i2cSlaveRx_func;
}

void i2cSetSlaveTransmitHandler(void (*i2cSlaveTx_func)(unsigned char receiveDataLength, unsigned char* recieveData)){
	i2cSlaveTransmit = i2cSlaveTx_func;
}

/*****************************************
 MASTER FUNCTIONS
******************************************/
void ipmb_send(unsigned char deviceAddr, unsigned char length, unsigned char *data){
			
	if ( ! ( AVR32_TWIM1.SR.txrdy ) )
	{
		// Re-init the master
		// Do a full reset of the transceiver
		AVR32_TWIM1.CR.swrst = 0x1;
		AVR32_TWIM1.CR.stop = 0x1;
		AVR32_TWIM1.CR.men = 0x1;
		twi_master_setup(&AVR32_TWIM1, &TWI_MASTER_OPTS);
	}
	
	//TWI_MASTER_OPTS.chip = deviceAddr >> 1;
	twim_write ( &AVR32_TWIM1, data, length, (deviceAddr & 0xFE) >> 1, 0x00 );
	
	//stat=twi_slave_setup( &AVR32_TWIS1, &TWI_SLAVE_OPTS, &TWI_CBS);
}

/*****************************************
 SLAVE FUNCTIONS
******************************************/
unsigned char nbpacketcnt = 0;

void i2c_slave_rx ( U8 pByteIn ){	
	if ( I2cReceiveDataIndex < MAX_BYTES_READ+15 ){		
		I2cReceiveData[I2cReceiveDataIndex++] = pByteIn;
	}else{
		I2cReceiveDataIndex = 1;
	}
	return;
}

unsigned char i2c_slave_tx(){
	//Not used - MMC must reply as I2C master as mentioned in the standard
	return 0xFF;
}

void i2c_slave_stop(){
	I2cReceiveData[0] = get_twis_addr() << 1;
	if(i2cSlaveReceive)
		i2cSlaveReceive(I2cReceiveDataIndex, I2cReceiveData);         // i2c receive is complete, call i2cSlaveReceive
	I2cReceiveDataIndex = 1;
	
	return;
}