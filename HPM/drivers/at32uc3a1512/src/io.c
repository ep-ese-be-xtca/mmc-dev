/*
 * io.c
 *
 * Created: 16/10/2015 09:05:09
 *  Author: jumendez
 */ 
#include <asf.h>
#include <math.h>

#include "../inc/pinout.h"

unsigned char get_signal(unsigned char id){
	return gpio_get_pin_value(id) ? HIGH:LOW; //(AVR32_GPIO.port[(int)floor((id)/32)].pvr & (1 << ((id)%32))) ? HIGH:LOW;
}

void set_signal(unsigned char id){
	gpio_set_pin_high(id); //AVR32_GPIO.port[(int)floor((id)/32)].ovrs = (1 << ((id)%32));
}

void clear_signal(unsigned char id){
	gpio_set_pin_low(id); //AVR32_GPIO.port[(int)floor((id)/32)].ovrc = (1 << ((id)%32));
}

void set_signal_dir(unsigned char id, unsigned char inout){
	if(inout){
		gpio_configure_pin 	(id, GPIO_DIR_INPUT); //AVR32_GPIO.port[(int)floor((id)/32)].oderc = 1 << ((id)%32);
	}else{
		gpio_configure_pin 	(id, GPIO_DIR_OUTPUT); //AVR32_GPIO.port[(int)floor((id)/32)].oders = 1 << ((id)%32);
	}
}

void init_port(void){
	//  AVR32_GPIO register allows controlling the AVR32 micrcontrolleur GPIOs
	//  datasheet: http://www.atmel.com/Images/doc32072.pdf (p. 381)
	//  -- ODER: Output Driver Enable Register (Read/Write)
	//		[val: 0] = Input	(oders register -> set bit)
	//		[val: 1] = Output	(oderc register -> clear bit)
	//	-- GPER: GPIO Enable Register (Read/Write)
	//		[val: 0] = Deactivate pin
	//		[val: 1] = Activate pin
	//	-- OVR: Output Value Register (Read/Write)
	//	-- PVR: Pin Value Register (Read)
	//	-- PUER: Pull-Up Enable Register (Read/Write)
	
	//AVR32_GPIO.port[(int)floor((FPGA_INIT_DONE)/32)].oders = 1 << ((FPGA_INIT_DONE)%32);
	//AVR32_GPIO.port[(int)floor((FPGA_INIT_DONE)/32)].gpers = 1 << ((FPGA_INIT_DONE)%32);	
	set_signal_dir(GA0, INPUT);
	set_signal_dir(GA1, INPUT);
	set_signal_dir(GA2, INPUT);
	set_signal_dir(GA_PULLUP, OUTPUT);
	
	set_signal_dir(MX0, OUTPUT);
	set_signal_dir(MX1, OUTPUT);
	set_signal_dir(MX2, OUTPUT);
	set_signal_dir(MX3, OUTPUT);
	
	set_signal_dir(LOCAL_BLUE_LED, OUTPUT);
	set_signal_dir(LOCAL_GREEN_LED, OUTPUT);
	set_signal_dir(LOCAL_RED_LED, OUTPUT);
	set_signal_dir(PWR_GPIO_ENABLE, OUTPUT);
	
	clear_signal(PWR_GPIO_ENABLE);
	clear_signal(MX0);
	clear_signal(MX1);
	clear_signal(MX2);
	clear_signal(MX3);
	
	
	set_signal_dir(EN0, OUTPUT);
	set_signal_dir(EN1, OUTPUT);
	set_signal_dir(EN2, OUTPUT);
	set_signal_dir(EN3, OUTPUT);
}