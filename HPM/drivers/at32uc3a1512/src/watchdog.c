/*
 * watchdog.c
 *
 * Created: 16/10/2015 09:04:30
 *  Author: jumendez
 */ 
#include <asf.h>

void watchdog_enable(){
	wdt_opt_t opt;	
	opt.us_timeout_period = 30000000;	//30s
	wdt_enable(&opt);
}

void watchdog_disable(){
	wdt_disable();
}

void watchdog_reset(){
	wdt_reenable();
}