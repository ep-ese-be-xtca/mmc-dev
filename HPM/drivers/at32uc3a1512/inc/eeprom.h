/*
 * eeprom.h
 *
 * Created: 15/10/2015 22:48:58
 *  Author: jumendez
 */ 


#ifndef EEPROM_H_
#define EEPROM_H_

#define SPI0_MEM_SEL 0

#define SPI_SPEED_12_5K 12500
#define SPI_SPEED_1M 1000000
#define SPI_SPEED_5M 5000000

#define SPI_MEM_CMD_READ  0x03
#define SPI_MEM_CMD_WRITE 0x02
#define SPI_MEM_CMD_RDSR  0x05
#define SPI_MEM_CMD_WRHF  0x07

#define SPI_MEM_WRHF_CLR_MASK   (1 << 0)
#define SPI_MEM_WRHF_SET_MASK   (1 << 1)
#define SPI_MEM_WRHF_READY_MASK  (1 << 5)
#define SPI_MEM_WRHF_TRANS_MASK  (1 << 3)
#define SPI_MEM_WRHF_LSTB_MASK  (1 << 6)
#define SPI_MEM_WRHF_USTB_MASK  (1 << 7)
#define SPI_MEM_WRHF_ALL_MASK   0xFC

void eeprom_init(void);
unsigned char EEPROM_status(void);
unsigned char EEPROM_writeEnable(void);
unsigned char EEPROM_writeDisable(void);
unsigned char EEPROM_status(void);

#endif /* EEPROM_H_ */