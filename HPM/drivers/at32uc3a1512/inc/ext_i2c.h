/*
 * ext_i2c.h
 *
 * Created: 17/10/2015 11:22:19
 *  Author: jumendez
 */ 


#ifndef EXT_I2C_H_
#define EXT_I2C_H_


// Constants
#define READ            0x01  // I2C READ bit

// Macros
//#define I2C_SDL_LO      cbi(LOCAL_I2C_SDA_PPORT, LOCAL_I2C_SDA_PIN)
//#define I2C_SDL_HI      sbi(LOCAL_I2C_SDA_PPORT, LOCAL_I2C_SDA_PIN)
//#define I2C_SCL_LO      cbi(LOCAL_I2C_SCL_PPORT, LOCAL_I2C_SCL_PIN)
//#define I2C_SCL_HI      sbi(LOCAL_I2C_SCL_PPORT, LOCAL_I2C_SCL_PIN)
#define I2C_SDL_LO      set_signal_dir(LOCAL_I2C_SDA, 0x00)    // Set pin as output and force a '0'
#define I2C_SDL_HI      set_signal_dir(LOCAL_I2C_SDA, 0x01)    // Set pin as input and release line
#define I2C_SCL_LO      set_signal_dir(LOCAL_I2C_SCL, 0x00)    // Set pin as output and force a '0'
#define I2C_SCL_HI      set_signal_dir(LOCAL_I2C_SCL, 0x01)    // Set pin as input and release line

#define I2C_SCL_TOGGLE  { HDEL; I2C_SCL_HI; HDEL; I2C_SCL_LO; }       //MJ: As these macros consist of several sub-commands it is better to put them into "{}"
#define I2C_START       { I2C_SCL_HI; HDEL; I2C_SDL_LO; HDEL; I2C_SCL_LO; HDEL; }             //MJ: Consequently they should be called without a ";" at the end
#define I2C_STOP        { I2C_SCL_HI; HDEL; I2C_SDL_HI; HDEL; I2C_SCL_LO; HDEL; }
#define HQDEL           { asm volatile("nop"); }      // i2c half quarter-bit delay
#define QDEL			{ HQDEL; HQDEL; }
#define HDEL            { QDEL; } //QDEL; }

#define ZERO_LEN	0
#define BYTE_LEN	1
#define SHORT_LEN	2

// Functions
void i2cswInit(void);                                                          // initialise I2C interface pins
unsigned char i2cPutbyte(unsigned char b);
unsigned char i2cGetbyte(unsigned int last);


#endif /* EXT_I2C_H_ */