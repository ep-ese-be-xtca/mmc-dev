/*
 * drivers.h
 *
 * Created: 14/10/2015 14:02:42
 *  Author: jumendez
 */ 


#ifndef DRIVERS_H_
#define DRIVERS_H_	
	
	/* Constants */
	#define MAX_BYTES_READ 60	//I2C buffer size for IPMB messages
	
	/*Include(s) for I2C extender(s)*/
	//#include "i2c_extender/pcf8574.h"
	
	/* Startup function */
		void boot_uC(void);
		void reset_uC(void);
		
	/* Flash memory interface */	
		unsigned char flash_write(unsigned char *data, unsigned char data_len);
		unsigned char flash_check(unsigned int length);
	
	/* EEPROM interface */
		void write_eeprom_byte(unsigned short addr, unsigned char data);
		unsigned char read_eeprom_byte(unsigned short addr);
	
	/* Startup FLAGs */
		void write_startup_flag(unsigned char flag);
		unsigned char get_startup_flag();
						
	/* IO monitoring and control */
		unsigned char get_signal(unsigned char id);
		void set_signal(unsigned char id);
		void clear_signal(unsigned char id);
		void set_signal_dir(unsigned char id, unsigned char inout);	//Output = 0x00 and Input = 0x01
		
	/* Watchdog feature */
		void watchdog_enable(void);
		void watchdog_disable(void);
		void watchdog_reset(void);
		
	/* Timer */
		void delay_ms(unsigned short time_ms);
		void delay_us(unsigned short time_us);	
		
	/* IPMB-L bus */
		void ipmb_i2c_configuration(unsigned char address, void (*ipmb_callback)(unsigned char receiveDataLength, unsigned char* recieveData));
		void ipmb_send(unsigned char deviceAddr, unsigned char length, unsigned char* data);
		unsigned char slave_i2c_poll();		//Only if I2C_POLLING_FUNC is defined
		int master_i2c_poll();				//Only if I2C_POLLING_FUNC is defined
		
	/* Debug */
		void debug(unsigned char *str);
		
	/*Includes - micro-controller dependent*/
	#ifdef ATMEGA128
		#include "atmega128/inc/pinout.h"
		//#include "atmega128/inc/addfeatures.h"
		
		#define PROGRAM_START_ADDR	0x0000
		#define I2C_POLLING_FUNC
	#endif
	
	#ifdef AT32UC3A
		#include <asf.h>
		#include "at32uc3a/inc/pinout.h"
		//#include "at32uc3a/inc/addfeatures.h"
				
		#define PROGRAM_START_OFFSET        0x0020000
		#define PROGRAM_START_ADDR         (AVR32_FLASH_ADDRESS + PROGRAM_START_OFFSET)
	#endif
	
	#ifdef AT32UC3A1512
		#include <asf.h>
		#include "at32uc3a1512/inc/pinout.h"
		//#include "at32uc3a/inc/addfeatures.h"
	
		#define PROGRAM_START_OFFSET        0x0020000
		#define PROGRAM_START_ADDR         (AVR32_FLASH_ADDRESS + PROGRAM_START_OFFSET)
	#endif

#endif /* DRIVERS_H_ */