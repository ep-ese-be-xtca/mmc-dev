/*
 * bootloader.h
 *
 * Created: 20/10/2014 10:02:15
 *  Author: jumendez
 */ 


#ifndef BOOTLOADER_H_
#define BOOTLOADER_H_

#define BUFFERSIZE         128
#define MEMORY_SIZE		   512//E2END //JM: Erasing all the memory take long time. I limit the size to 512 bytes

#if BUFFERSIZE > SPM_PAGESIZE
	#error "BUFFERSIZE should be less than SPM_PAGESIZE"
#else
	#if (SPM_PAGESIZE /BUFFERSIZE * BUFFERSIZE) != SPM_PAGESIZE
		#error "Result of (BUFFERSIZE / SPM_PAGESIZE) is not a Integer!"
		#error "Please check and set 'BUFFERSIZE/SPM_PAGESIZE' Macro again!"
	#endif
#endif

void flash_init();
unsigned char boot_program_page (uint32_t page, uint8_t *buf);

#endif /* BOOTLOADER_H_ */