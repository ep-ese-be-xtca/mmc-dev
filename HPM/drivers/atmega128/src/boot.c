/*
 * boot.c
 *
 * Created: 14/10/2015 15:23:29
 *  Author: jumendez
 */ 
#include <avr/io.h>
#include <avr/wdt.h>

#include "../inc/iodeclaration.h"
#include "../inc/avrlibdefs.h"
#include "../inc/i2c.h"
#include "../inc/bootloader.h"

void boot_uC(){
	
	__asm__ __volatile__("cli": :);
	SFIOR |= 0x04;					// disable internal pull-up resistors
	
	init_port();					// I/Os init
	i2cInit();                      // initialize i2c function library
	flash_init();					// Initialize the bootloader write flash feature
	
	sei();							// enable all interrupts
}

void reset_uC(){
	wdt_disable();
	wdt_enable(WDTO_15MS);
	while(1);
}
