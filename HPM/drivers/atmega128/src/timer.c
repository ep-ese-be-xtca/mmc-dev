/*! \file timer.c \ Timer functions */
//*****************************************************************************
//
// File Name	: 'timer.c'
// Title		: Timer functions
// Author		: Unknown
// Modified by  : Markus joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************
#include "../inc/global.h"
#include "../inc/avrlibdefs.h"

// Delay for a minimum of <us> microseconds
// The time resolution is dependent on the time the loop takes
// e.g. with 4Mhz and 5 cycles per loop, the resolution is 1.25 us 
//***********************/
void delay_us(unsigned short time_us)     //Called from ipmi_if.c and fru.c
//***********************/
{
	unsigned short delay_loops;
	register unsigned short i;     

	// one loop takes 5 cpu cycles 
	delay_loops = (time_us + 3) / 22 * 3 * CYCLES_PER_US; // +3 for rounding up (dirty)  //MJ: "22 * 3" is the result of a manual calibration of the delay

	for (i=0; i < delay_loops; i++)
	    asm volatile("nop");          //MJ: Without the "nop" there is the risk that the loop gets removed by the optimizer
}

void delay_ms(unsigned short time_ms)     //Called from ipmi_if.c and fru.c
//***********************/
{
	unsigned short delay_loops;
	register unsigned short i,j;

	// one loop takes 5 cpu cycles
	delay_loops = (time_ms + 3) / 22 * 3 * CYCLES_PER_US; // +3 for rounding up (dirty)  //MJ: "22 * 3" is the result of a manual calibration of the delay

	for(j=0; j<1000; j++){
		for (i=0; i < delay_loops; i++)
			asm volatile("nop");          //MJ: Without the "nop" there is the risk that the loop gets removed by the optimizer
	}
}