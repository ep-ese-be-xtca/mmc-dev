#ifndef FRU_INFO_H
#define FRU_INFO_H

#include "../fru.h"
#include "../languages.h"

/*********************************************
 * Common defines
 *********************************************/
#define LANG_CODE		ENGLISH
#define FRU_FILE_ID		"CoreFRU"	//Allows knowing the source of the FRU present in the memory

#define BOARD_INFO_AREA_ENABLE
#define PRODUCT_INFO_AREA_ENABLE
#define MULTIRECORD_AREA_ENABLE

/*********************************************
 * Board information area
 *********************************************/
#define BOARD_MANUFACTURER		"CERN"
#define	BOARD_NAME				"AMC-Loadboard"
#define BOARD_SN				"000000001"
#define BOARD_PN				"AMC-L"
 
/*********************************************
 * Product information area
 *********************************************/
#define PRODUCT_MANUFACTURER	"CERN"
#define PRODUCT_NAME			"AMC LoadBoard"
#define PRODUCT_PN				"00001"
#define PRODUCT_VERSION			"v3"
#define PRODUCT_SN				"00001"
#define PRODUCT_ASSET_TAG		"No tag"
 
/*********************************************
 * AMC: Point to point connectivity record
 *********************************************/
/** No AMC point to point connectivity for ALB*/

/*********************************************
 * AMC: Point to point clock record
 *********************************************/
#define USER_CLK1		6

#define AMC_CLOCK_CONFIGURATION_LIST																		\
	DIRECT_CLOCK_CONNECTION(TCLKA, CIPMC, NO_PLL, RECEIVER, UNSPEC_FAMILY, 0, KHz(8), KHz(7), KHz(9))		\
	INDIRECT_CLOCK_CONNECTION(USER_CLK1, CIPMC, PLL, RECEIVER, TCLKA)										\
	DIRECT_CLOCK_CONNECTION(TCLKB, CIPMC, PLL, RECEIVER, UNSPEC_FAMILY, 0, KHz(2), KHz(1), KHz(3))			\
	DIRECT_CLOCK_CONNECTION(FCLKA, CIPMC, PLL, RECEIVER, UNSPEC_FAMILY, 0, MHz(12.8), MHz(11), MHz(13))

/**********************************************
 * PICMG: Module current record
 **********************************************/
#define MODULE_CURRENT_RECORD		 current_in_ma(6500)	//	current_in_ma(max_current_in_mA)
	
#endif