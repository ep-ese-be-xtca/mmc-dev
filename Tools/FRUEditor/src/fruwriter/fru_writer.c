#include <mtca.h>
#include <string.h>

int print_fru_info(unsigned char *ip, unsigned char *username, unsigned char *password, unsigned char addr);
int resetFRU(unsigned char *ip, unsigned char *username, unsigned char *password, unsigned char amc_slot_number);
int writeFRU(unsigned char *ip, unsigned char *username, unsigned char *password, unsigned char amc_slot_number);

unsigned char filename[255];

int main(int argc, char ** argv)
{
	int ret=0, i;
	unsigned char data[25];
	
	unsigned char ip[25];
	unsigned char username[25];
	unsigned char password[25];
	unsigned char slot;
	
	if(argc >= 5){
		strcpy(ip,argv[1]);
		strcpy(username,argv[2]);
		strcpy(password,argv[3]);
		slot = atoi(argv[4]);
		strcpy(filename,argv[5]);
	}else
		return -1;
	
	printf("IP: %s \n",ip);
	printf("Username: %s \n",username);
	printf("Password: %s \n",password);
	printf("AMC Slot: %d \n",slot);
	
	//Open session to MCH (Only one session at a time)
	struct ipmi_intf *intf = open_lan_session(ip, 
						  username, 
						  password, 
						  0, 		//No target specified (Default: MCH)
			  			  0,		//No transit addr specified (Default: 0) 
						  0,		//No target channel specified (Default: 0)
						  0);		//No transit channel specified (Default: 0)
									
	struct ipmi_rs *rsp = send_ipmi_cmd(intf, 0x06, 0x01, NULL, 0);	
	
	if(rsp == NULL){
		printf("{MCH - Get Device ID} Error : command failed \n");
		return -1;
	}else{
		if(rsp->ccode){
			printf("{MCH GET_DEVICE_ID} Error: Completion Code 0x%02x expected 0x00 \n", rsp->ccode);
			return -2;
		}
	}
	
	intf->close(intf);
	
	//Open session to AMC2 (Only one session at a time)
	intf = open_lan_session(ip,
				username,
				password,
				(0x70+2*slot),	//Target AMC1 (addr = 0x74)
				0x82,		//Transit addr = MCH addr
				7,		//Target channel (Bridge request to remote target address)
				0);
	
	rsp = send_ipmi_cmd(intf, 0x06, 0x01, NULL, 0);		
	if(rsp == NULL){
		printf("{AMC%d - Get Device ID} Error : command failed \n",slot);
		return -1;
	}else{
		if(rsp->ccode){
			printf("{AMC%d GET_DEVICE_ID} Error: Completion Code 0x%02x expected 0x00 \n", slot, rsp->ccode);
			return -2;
		}
	}
	intf->close(intf);
	
	if(resetFRU(ip, username, password,slot))	return -1;
	if(writeFRU(ip, username, password,slot))	return -1;
	if(print_fru_info(ip, username, password,slot)) return -1;

	return 0;
}

int resetFRU(unsigned char *ip, unsigned char *username, unsigned char *password, unsigned char amc_slot_number){
	
	unsigned char len, i, offset;
	
	unsigned char data[25];
	struct ipmi_rs *rsp;
	
	struct ipmi_intf *intf = open_lan_session(ip, 
						  username, 
						  password, 
						  (0x70+2*amc_slot_number),		//No target specified (Default: MCH)
						  0x82,					//No transit addr specified (Default: 0) 
						  7,					//No target channel specified (Default: 0)
						  0);
									
	data[0] = 0x00;
	data[2] = 0;
	data[3] = 0;
	
	for(i=0; i < 255; i++){
		
		data[1] = i;
		
		rsp = send_ipmi_cmd(intf, IPMI_NETFN_STORAGE, 0x12, data, 4);	
		if(rsp == NULL){
			printf("{Reset fru} Error : command failed \n");
			return -1;
		}else{
			if(rsp->ccode){
				printf("{Reset fru byte %d} Completion Code : %x \n", i, rsp->ccode);
				return -1;
			}
		}
	}
	
	intf->close(intf);
	return 0;
	
}

int writeFRU(unsigned char *ip, unsigned char *username, unsigned char *password, unsigned char amc_slot_number){
	
	unsigned char len, i, offset;
	FILE *binary = NULL;
	
	unsigned char data[25];
	struct ipmi_rs *rsp;
	
	struct ipmi_intf *intf = open_lan_session(ip, 
									username, 
									password, 
									(0x70+2*amc_slot_number),			//No target specified (Default: MCH)
									0x82,								//No transit addr specified (Default: 0) 
									7,									//No target channel specified (Default: 0)
									0);
		
	binary = fopen(filename,"rb");
	if(binary == NULL){
		printf("ERROR: fopen() \n");
		intf->close(intf);
		return -1;
	}
	data[0] = 0x00;
	data[2] = 0;
	data[3] = 0;
	
	i=0;
	
	while(fread(&data[3], 1, 1, binary) == 1){
		
		if(i >= 255){
			data[2] = i/255;
			data[1] = i%255;
		}else{
			data[1] = i;
		}
			
		//printf("wirte {%d, val=0x%02x} : %d(MSB) %d(LSB) (",i,data[3],data[2],data[1]);
		
		rsp = send_ipmi_cmd(intf, IPMI_NETFN_STORAGE, 0x12, data, 4);	
		if(rsp == NULL){
			printf("{Write fru} Error : command failed \n");
			return -1;
		}else{
			if(rsp->ccode){
				printf("{Write fru byte %d} Completion Code : %x \n", i, rsp->ccode);
				return -1;
			}
		}
		
		i++;
	}
	
	intf->close(intf);
	return 0;
}

//Print FRU info example function
int print_fru_info(unsigned char *ip, unsigned char *username, unsigned char *password, unsigned char amc_slot_number){

	unsigned char internal_use_area_offset;
	unsigned char chassis_info_area_offset;
	unsigned char board_area_offset;
	unsigned char product_info_area_offset;
	unsigned char multi_record_area_offset;
	
	unsigned char len, i, offset;
	
	unsigned char data[25];
	
	printf("\n===== Print FRU info (AMC %d) =====\n",amc_slot_number);
	
	struct ipmi_intf *intf = open_lan_session(ip, 
									username, 
									password, 
									(0x70+2*amc_slot_number),			//No target specified (Default: MCH)
									0x82,								//No transit addr specified (Default: 0) 
									7,									//No target channel specified (Default: 0)
									0);									//No transit channel specified (Default: 0)
	
	data[0] = 0x00;
	data[1] = 1;
	data[2] = 0;
	data[3] = 1;
	
	struct ipmi_rs *rsp = send_ipmi_cmd(intf, IPMI_NETFN_STORAGE, 0x11, data, 4);	
	
	if(rsp == NULL){
		printf("Error : read fru cmd failed \n");
		return -1;
	}
	
	internal_use_area_offset = rsp->data[1];
	
	data[1] = 2;
	rsp = send_ipmi_cmd(intf, IPMI_NETFN_STORAGE, 0x11, data, 4);	
	
	if(rsp == NULL){
		printf("Error : read fru cmd failed \n");
		return -1;
	}
	
	chassis_info_area_offset = rsp->data[1];
	
	data[1] = 3;
	rsp = send_ipmi_cmd(intf, IPMI_NETFN_STORAGE, 0x11, data, 4);	
	
	if(rsp == NULL){
		printf("Error : read fru cmd failed \n");
		return -1;
	}
	
	board_area_offset = rsp->data[1];
	
	data[1] = 4;
	rsp = send_ipmi_cmd(intf, IPMI_NETFN_STORAGE, 0x11, data, 4);	
	
	if(rsp == NULL){
		printf("Error : read fru cmd failed \n");
		return -1;
	}
	
	product_info_area_offset = rsp->data[1];
	
	data[1] = 5;
	rsp = send_ipmi_cmd(intf, IPMI_NETFN_STORAGE, 0x11, data, 4);	
	
	if(rsp == NULL){
		printf("Error : read fru cmd failed \n");
		return -1;
	}
	
	multi_record_area_offset = rsp->data[1];
		
	printf("Internal use area offset : %d \n", internal_use_area_offset*8);
	printf("Chassis info area offset : %d\n",chassis_info_area_offset*8);
	printf("Board area offset : %d \n",board_area_offset*8);
	printf("Product info area offset : %d \n", product_info_area_offset*8);
	printf("Multi record area offset : %d \n\n", multi_record_area_offset*8);
	
	if(board_area_offset != 0){
	
		offset = (board_area_offset*8)+6;
		
		if(offset >= 255){data[2] = offset/255; data[1] = offset - (data[2]*255);}
		else{ data[1] = offset; }
				
		rsp = send_ipmi_cmd(intf, IPMI_NETFN_STORAGE, 0x11, data, 4);
		if(rsp == NULL){
			printf("Error : read fru cmd failed \n");
			intf->close(intf);
			return -1;
		}
		
		len = (rsp->data[1] & 0x3F);
		offset++;
		
		printf("Board manufacturer: ");
		for(i=0; i < len; i++){
			if(offset+i >= 255){data[2] = (offset+i)/255; data[1] = (offset+i) - (data[2]*255);}
			else{ data[1] = offset+i; }
			
			rsp = send_ipmi_cmd(intf, IPMI_NETFN_STORAGE, 0x11, data, 4);
			if(rsp == NULL){
				printf("Error : read fru cmd failed \n");
				intf->close(intf);
				return -1;
			}
			
			printf("%c",rsp->data[1]);
		}
		printf("\n");
		
		offset += len;
		
		if(offset >= 255){data[2] = offset/255; data[1] = offset - (data[2]*255);}
		else{ data[1] = offset; }
				
		rsp = send_ipmi_cmd(intf, IPMI_NETFN_STORAGE, 0x11, data, 4);
		if(rsp == NULL){
			printf("Error : read fru cmd failed \n");
			intf->close(intf);
			return -1;
		}
		
		len = (rsp->data[1] & 0x3F);
		offset++;
		
		printf("Board product name: ");
		for(i=0; i < len; i++){
			if(offset+i >= 255){data[2] = (offset+i)/255; data[1] = (offset+i) - (data[2]*255);}
			else{ data[1] = offset+i; }
			
			rsp = send_ipmi_cmd(intf, IPMI_NETFN_STORAGE, 0x11, data, 4);
			if(rsp == NULL){
				printf("Error : read fru cmd failed \n");
				intf->close(intf);
				return -1;
			}
			
			printf("%c",rsp->data[1]);
		}
		printf("\n");
	}
	intf->close(intf);

	return 0;
}
