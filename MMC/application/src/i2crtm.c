/*! \file i2rtm.c \brief Software I2C interface using port pins. */
//*****************************************************************************
//
// File Name	: 'i2rtm.c'
// Title		: Software I2C interface to RTM module
// Author		: Vahan Petrosyan
// Created		: 10/19/2010
// Target MCU	: Atmel AVR series
//
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
//*****************************************************************************
#ifdef USE_RTM
	#include "../../drivers/drivers.h"

	#include "../../user/user_code/config.h"

	#include "../inc/project.h"
	#include "../inc/ipmi_if.h"
	#include "../inc/i2crtm.h"

	//! Initialise I2C communication
	//******************/
	unsigned char i2crtmInit(void)    //Called from rtm_mng.c
	//******************/
	{
		unsigned char data = 0x01;
	
		set_signal(RTM_I2C_ENABLE);		// In our case activating the RTM I2C only consist into enabling the I2C buffer. Then the same I2C pins are used as for the rest.
		delay_ms(10);					//Wait for ready
	
		ext_i2c_send(RTM_IO_PORTS_ADDR, 0, 0, 1, &data);
		ext_i2c_received(RTM_IO_PORTS_ADDR, 0, 0, 1, &data);

		if(data == 0xFF)	return 0xFF;	//Error
	
		return 0x00;
	}

	unsigned char rtm_fru_read_byte(unsigned short address){
		unsigned char data;
		ext_i2c_received(RTM_EEPROM_ADDR, address, 2, 1, &data);
		return data;
	}

	unsigned char rtm_fru_data_read(unsigned short address, unsigned char len, unsigned char* data){

		//if (address >= RTM_FRU_SIZE)
		//	return(0xff);

		if (len > MAX_BYTES_READ)
			len = MAX_BYTES_READ;

		//if ((address + len) > RTM_FRU_SIZE)
		//	len = RTM_FRU_SIZE - address;

		// write register address

		*data++ = len;
		ext_i2c_received(RTM_EEPROM_ADDR, address, 2, len, data); //MJ: CERN special: AMC and RTM use the same I2C bus

		return(len + 1);
	}

	//************************************************************************************/
	unsigned char write_rtm_eeprom_page(unsigned short base_addr, unsigned char length, unsigned char *data, unsigned short after_write_delay)   //called from load_rtm_eeprom in this file
	//************************************************************************************/
	{
	
		unsigned char eeprom_data[32];
		unsigned char page = 0;
		unsigned char i = 0;
	
		if(base_addr+length > 0x80)	return 0xFF;
				
		if(base_addr >= 0x00 && base_addr < 0x20)
		  page = 0;
		else if(base_addr >= 0x20 && base_addr < 0x40)
		  page = 1;
		else if(base_addr >= 0x40 && base_addr < 0x60)
		  page = 2;
		else if(base_addr >= 0x60 && base_addr < 0x80)
		  page = 3;
  
		while(length > 0){
			ext_i2c_received(RTM_EEPROM_ADDR, 0x20*page, 2, 32, eeprom_data);		//Get page values
			while(base_addr-(0x20*page) < 0x20 && length > 0){
				eeprom_data[base_addr-(0x20*page)] = data[i++];
				base_addr++;
				length--;
			}
		
			ext_i2c_send(RTM_EEPROM_ADDR, 0x20*page, 2, 32, eeprom_data);
			while (!ext_i2c_poll(RTM_EEPROM_ADDR));
		
		
			delay_ms(after_write_delay);
		
			page++;
		}
	
		return 0x00;
	}
#endif