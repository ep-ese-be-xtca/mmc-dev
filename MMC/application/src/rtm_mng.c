/*! \file rtm_mng.c \brief Software I2C interface using port pins. */
//*****************************************************************************
//
// File Name	: 'rtm_mng.c'
// Title		: main management functions for RTM module
// Author		: Vahan Petrosyan
// Created		: 10/19/2010
// Target MCU	: Atmel AVR series
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//*****************************************************************************
#include "../../drivers/drivers.h"

#include "../../user/user_code/config.h"
#include "../../user/user_code/sensors.h"

#include "../inc/project.h"
#include "../inc/led.h"
#include "../inc/sensors.h"
#include "../inc/sdr.h"
#include "../inc/ipmi_if.h"
#include "../inc/i2crtm.h"
#include "../inc/rtm_frueditor.h"
#include "../inc/sensor_macro.h"
#include "../inc/rtm_mng.h"

#ifdef USE_RTM

//Global variables
unsigned char uRTM_state = NOT_PRESENT;
unsigned short rtm_deactivation_flag = 0;

unsigned char init_uRTM(){
	unsigned char i;
	
	set_signal(RTM_3V3_ENABLE);			//Active management power
	rtm_set_payload_power(0);			//De-active Payload power
	
	delay_ms(200);
	
	if(i2crtmInit()){		//uRTM init failed
		return 0xFF;
	}
	
	for(i=0; i < get_num_of_led(RTM); i++){
		reset_led(RTM, i);
	}
	
	return 0x00;
}

unsigned char handle_state;
unsigned char event_to_send = 0x00;
unsigned char event_send_err = 0x00;
unsigned char event_assert_deassert = 0x00;

void manage_uRTM(){
	unsigned char hotswap_event, i;
	
	
	if(event_send_err){
		event_send_err = ipmi_event_send(RTM_HOT_SWAP_SENSOR, event_assert_deassert, &event_to_send, 1);
		return;
	}
	
	handle_state = get_PCF8574_bit(RTM_IO_PORTS_ADDR, RTM_HANDLE_PIN);
	if(!handle_state){
		set_sensor_value(RTM_HOT_SWAP_SENSOR, get_sensor_value(RTM_HOT_SWAP_SENSOR) & 0xFD);	//Clear hot swap opened and quiesced bits
		set_sensor_value(RTM_HOT_SWAP_SENSOR, get_sensor_value(RTM_HOT_SWAP_SENSOR) | 0x01);	//Set hot swap closed bit bit		
	}else{		
		set_sensor_value(RTM_HOT_SWAP_SENSOR, get_sensor_value(RTM_HOT_SWAP_SENSOR) & 0xFE);	//Clear hot swap closed bit
		set_sensor_value(RTM_HOT_SWAP_SENSOR, get_sensor_value(RTM_HOT_SWAP_SENSOR) | 0x02);	//Set hot swap openend bit
	}
	
	if(uRTM_state != NOT_PRESENT && get_signal(RTM_PS)){										//uRTM absent
		set_sensor_value(RTM_HOT_SWAP_SENSOR, get_sensor_value(RTM_HOT_SWAP_SENSOR) & 0xDF);	//Clear uRTM present bit
		set_sensor_value(RTM_HOT_SWAP_SENSOR, get_sensor_value(RTM_HOT_SWAP_SENSOR) | 0x40);	//Set uRTM absent bit
		
		local_led_control(RTM, GREEN_LED, LED_OFF);
		
		hotswap_event = 0x06;	//uRTM present
		event_to_send = hotswap_event;
		event_assert_deassert = ASSERTION_EVENT;
		event_send_err = ipmi_event_send(RTM_HOT_SWAP_SENSOR, ASSERTION_EVENT, &hotswap_event, 1);
		
		for (i = (AMC_SENSOR_CNT+DRIVERS_SENSORS_CNT); i < (AMC_SENSOR_CNT+DRIVERS_SENSORS_CNT+RTM_SENSOR_CNT); i++)
			disable_sensor(i);
		
		uRTM_state = NOT_PRESENT;
				
		clear_signal(RTM_I2C_ENABLE);
		clear_signal(RTM_3V3_ENABLE);
		rtm_set_payload_power(0);			//De-active Payload power
	}
	
	
	else if(uRTM_state == NOT_PRESENT && !get_signal(RTM_PS)){			//uRTM have just been inserted
		init_uRTM();												//uRTM initialization
		
		local_led_control(RTM, GREEN_LED, LED_OFF);
		
		
		watchdog_disable();
		#ifdef FORCE_URTM_FRU_WRITE
			write_rtm_fru_binary();
		#else
			if(rtm_fru_read_byte(0) == 0xFF)
				write_rtm_fru_binary();
		#endif
		watchdog_enable();
		
		
		local_led_control(RTM, RED_LED, LED_OFF);					//Turn the uRTM red led OFF
		
		//Set RTM HotSwap Sensor value and send event
		enable_sensor(RTM_HOT_SWAP_SENSOR);	//Enable Hot swap sensor
		
		set_sensor_value(RTM_HOT_SWAP_SENSOR, get_sensor_value(RTM_HOT_SWAP_SENSOR) & 0xBF);	//Clear uRTM absent bit
		set_sensor_value(RTM_HOT_SWAP_SENSOR, get_sensor_value(RTM_HOT_SWAP_SENSOR) | 0x20);	//Set uRTM present bit
		
		uRTM_state = PRESENT;										//uRTM is detected
		hotswap_event = 0x05;	//uRTM present
		event_to_send = hotswap_event;
		event_assert_deassert = ASSERTION_EVENT;
		event_send_err =	ipmi_event_send(RTM_HOT_SWAP_SENSOR, ASSERTION_EVENT, &hotswap_event, 1);
	}
	
	else if(uRTM_state == PRESENT || uRTM_state == NON_COMPATIBLE){
		//Check compatibility
		local_led_control(RTM, GREEN_LED, LED_OFF);
		
		if(rtm_check_compatibility()){
			set_sensor_value(RTM_HOT_SWAP_SENSOR, get_sensor_value(RTM_HOT_SWAP_SENSOR) | 0x80);	//Set uRTM compatible bit
			hotswap_event = 0x07;	//uRTM compatible
			event_to_send = hotswap_event;
			event_assert_deassert = ASSERTION_EVENT;
			event_send_err =	ipmi_event_send(RTM_HOT_SWAP_SENSOR, ASSERTION_EVENT, &hotswap_event, 1);

			uRTM_state = COMPATIBLE;
		}else{
			set_sensor_value(RTM_HOT_SWAP_SENSOR, get_sensor_value(RTM_HOT_SWAP_SENSOR) & 0x7F);	//Clear uRTM compatible bit
			hotswap_event = 0x08;	//uRTM non compatible
			event_to_send = hotswap_event;
			event_assert_deassert = ASSERTION_EVENT;
			event_send_err =	ipmi_event_send(RTM_HOT_SWAP_SENSOR, ASSERTION_EVENT, &hotswap_event, 1);

			uRTM_state = NON_COMPATIBLE;			
		}
	}
	
	else if(uRTM_state == COMPATIBLE){
		
		local_led_control(RTM, GREEN_LED, LED_OFF);
		if(!handle_state){
			set_sensor_value(RTM_HOT_SWAP_SENSOR, get_sensor_value(RTM_HOT_SWAP_SENSOR) & 0xFB);	//Clear uRTM quiesced bit
			hotswap_event = 0x00;	//Handle closed
			event_to_send = hotswap_event;
			event_assert_deassert = ASSERTION_EVENT;
			event_send_err =	ipmi_event_send(RTM_HOT_SWAP_SENSOR, ASSERTION_EVENT, &hotswap_event, 1);	
		
			for (i = (AMC_SENSOR_CNT+DRIVERS_SENSORS_CNT); i < (AMC_SENSOR_CNT+DRIVERS_SENSORS_CNT+RTM_SENSOR_CNT); i++)	//Enable remaining sensors
				enable_sensor(i);
			
			uRTM_state = ACTIVATION;				
		}
	}
	
	else if(uRTM_state == ACTIVATED){
		
		local_led_control(RTM, GREEN_LED, LED_ON);
		
		if(handle_state){			
			local_led_control(RTM, GREEN_LED, LED_OFF);		
			hotswap_event = 0x01;	//Handle opened
			event_to_send = hotswap_event;
			event_assert_deassert = ASSERTION_EVENT;
			event_send_err =	ipmi_event_send(RTM_HOT_SWAP_SENSOR, ASSERTION_EVENT, &hotswap_event, 1);
		
			uRTM_state = DEACTIVATED;
		}
	}
	
	else if(uRTM_state == DEACTIVATED){
		local_led_control(RTM, GREEN_LED, LED_OFF);
		if(get_rtm_deactivation_flag()){
			set_sensor_value(RTM_HOT_SWAP_SENSOR, get_sensor_value(RTM_HOT_SWAP_SENSOR) | 0x04);	//Set uRTM present bit
			hotswap_event = 0x02;	//uRTM quiesced
			event_to_send = hotswap_event;
			event_assert_deassert = ASSERTION_EVENT;
			event_send_err =	ipmi_event_send(RTM_HOT_SWAP_SENSOR, ASSERTION_EVENT, &hotswap_event, 1);
			
			uRTM_state = COMPATIBLE;		
		}
	}
}

unsigned char get_rtm_state(){	return uRTM_state;		}
	
unsigned short get_fru_size(){		
	unsigned short size = 0;
	unsigned char header[8];
	
	ext_i2c_received(RTM_EEPROM_ADDR, 0x00, 2, 8, header);
	
	if(header[5]){
		size += ((unsigned short)header[5])*8;
		
		while(!(rtm_fru_read_byte(size+1) & 0x80)){
			size += rtm_fru_read_byte(size+2) + 5;
		}
		size += rtm_fru_read_byte(size+2) + 5;
	}
	
	return size;
}
	
void calcul_fru_size(){
	
}

unsigned char rtm_set_payload_power(unsigned char cmd){
	if(cmd == 1){	uRTM_state = ACTIVATED;		clear_signal(RTM_12V_ENABLE);		return 0x00; }		//Enable payload power (Clear signal RTM_12V_ENABLE = Active Payload Power)
	else if(cmd == 0){		set_signal(RTM_12V_ENABLE);	return 0x00; }				//Disable payload power (Set signal RTM_12V_ENABLE = Deactive Payload Power)
		
	return 0xFF;
}

	unsigned char buf[20];
	
unsigned char rtm_check_compatibility(){
	unsigned short size = 0, i;
	unsigned char header[8];

	ext_i2c_received(RTM_EEPROM_ADDR, 0x00, 2, 8, header);
	
	if(header[5]){
		size += ((unsigned short)header[5])*8;
		
		while(!(rtm_fru_read_byte(size+1) & 0x80)){
			
			for(i=0; i<20; i++){
				buf[i] = rtm_fru_read_byte(size+i);
			}
			
			if(	(rtm_fru_read_byte(size+2) + 5) == 18																&& /* Compatibility record length = 18 */
				(rtm_fru_read_byte(size))		== 0xC0																&& /* Compatibility record type = 0xC0 */
				(rtm_fru_read_byte(size+5))		== 0x5A																&& /* Compatibility record manufacturer (PICMG) = 0x00315A */
				(rtm_fru_read_byte(size+6))		== 0x31																&& /* Compatibility record manufacturer (PICMG) = 0x00315A */
				(rtm_fru_read_byte(size+7))		== 0x00																&& /* Compatibility record manufacturer (PICMG) = 0x00315A */
				(rtm_fru_read_byte(size+8))		== 0x30																&& /* Compatibility record ID = 0x30 */
				(rtm_fru_read_byte(size+10))	== 0x03																&& /* Identifier type (OEM) = 0x03 */
				(rtm_fru_read_byte(size+11))	== IPMI_MSG_MANU_ID_LSB												&& /* Check IANA compatibility */
				(rtm_fru_read_byte(size+12))	== IPMI_MSG_MANU_ID_B2												&& /* Check IANA compatibility */
				(rtm_fru_read_byte(size+13))	== IPMI_MSG_MANU_ID_MSB												&& /* Check IANA compatibility */
				(rtm_fru_read_byte(size+14))	== (unsigned char)((unsigned long)COMPATIBILITY_IDENTIFIER & (unsigned long)0x000000FF)			&& /* Check OEM ID compatibility */
				(rtm_fru_read_byte(size+15))	== (unsigned char)(((unsigned long)COMPATIBILITY_IDENTIFIER & (unsigned long)0x0000FF00) >> 8)	&& /* Check OEM ID compatibility */
				(rtm_fru_read_byte(size+16))	== (unsigned char)(((unsigned long)COMPATIBILITY_IDENTIFIER & (unsigned long)0x00FF0000) >> 16)	&& /* Check OEM ID compatibility */
				(rtm_fru_read_byte(size+17))	== (unsigned char)(((unsigned long)COMPATIBILITY_IDENTIFIER & (unsigned long)0xFF000000) >> 24))	   /* Check OEM ID compatibility */
				return 0xFF;	//Compatible
				
			size += rtm_fru_read_byte(size+2) + 5;	//Else, go to the next record
		}
		for(i=0; i<20; i++){
				buf[i] = rtm_fru_read_byte(size+i);
			}
			
		//Check last record
		if(	(rtm_fru_read_byte(size+2) + 5) == 18																&& /* Compatibility record length = 18 */
			(rtm_fru_read_byte(size))		== 0xC0																&& /* Compatibility record type = 0xC0 */
			(rtm_fru_read_byte(size+5))		== 0x5A																&& /* Compatibility record manufacturer (PICMG) = 0x00315A */
			(rtm_fru_read_byte(size+6))		== 0x31																&& /* Compatibility record manufacturer (PICMG) = 0x00315A */
			(rtm_fru_read_byte(size+7))		== 0x00																&& /* Compatibility record manufacturer (PICMG) = 0x00315A */
			(rtm_fru_read_byte(size+8))		== 0x30																&& /* Compatibility record ID = 0x30 */
			(rtm_fru_read_byte(size+10))	== 0x03																&& /* Identifier type (OEM) = 0x03 */
			(rtm_fru_read_byte(size+11))	== IPMI_MSG_MANU_ID_LSB												&& /* Check IANA compatibility */
			(rtm_fru_read_byte(size+12))	== IPMI_MSG_MANU_ID_B2												&& /* Check IANA compatibility */
			(rtm_fru_read_byte(size+13))	== IPMI_MSG_MANU_ID_MSB												&& /* Check IANA compatibility */
			(rtm_fru_read_byte(size+14))	== (unsigned char)((unsigned long)COMPATIBILITY_IDENTIFIER & (unsigned long)0x000000FF)			&& /* Check OEM ID compatibility */
			(rtm_fru_read_byte(size+15))	== (unsigned char)(((unsigned long)COMPATIBILITY_IDENTIFIER & (unsigned long)0x0000FF00) >> 8)	&& /* Check OEM ID compatibility */
			(rtm_fru_read_byte(size+16))	== (unsigned char)(((unsigned long)COMPATIBILITY_IDENTIFIER & (unsigned long)0x00FF0000) >> 16)	&& /* Check OEM ID compatibility */
			(rtm_fru_read_byte(size+17))	== (unsigned char)(((unsigned long)COMPATIBILITY_IDENTIFIER & (unsigned long)0xFF000000) >> 24))	   /* Check OEM ID compatibility */
			return 0xFF;	//Compatible
	}else
		return 0x00;	//0x00 - Non compatible
	
	return 0x00;	//0xFF - Non Compatible
}

void set_rtm_deactivation_flag(){
	rtm_deactivation_flag = 1;
}

unsigned char get_rtm_deactivation_flag(){
	unsigned char tmp = rtm_deactivation_flag;
	rtm_deactivation_flag = 0;
	return tmp;
}
#endif