/*
 * hotswap_sensor.c
 *
 * Created: 22/05/2017 11:24:33
 *  Author: jumendez
 */

#include "../../user/user_code/config.h"

#include "../inc/ipmi_if.h"
#include "../inc/sdr.h"
#include "../inc/sensors.h"
#include "../inc/hotswap_sensor.h"

signed char amc_hotswap_sdr[] = HOTSWAP_SDR;

unsigned char update_hotswap_value(){
	//Nothing to do
	return 0;
}

unsigned char get_hotswap_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset){
	
	unsigned int j;
	unsigned char read_len = 0;	
	
	if(sensor_id != HOT_SWAP_SENSOR) return 0x00;
	
	for(j=0; j < len && (j+offset) < sizeof(amc_hotswap_sdr); j++, read_len++){
		byte[j] = amc_hotswap_sdr[j+offset];
	}
	
	return read_len;
}

/** Generates the hot swap sensor record according to the
	section 37.1 of the IPMI v.1.5 standard */
unsigned char init_hotswap_sdr(unsigned char sdr_id, unsigned char sensor_id, unsigned char entity_inst, unsigned char entity_id, unsigned char owner_id){
	
	amc_hotswap_sdr[0] = 0x01;							//Record ID [LSB] (0x01)
	amc_hotswap_sdr[1] = 0x00;							//Record ID [MSB] (0x00)
	
	amc_hotswap_sdr[4] = sizeof(amc_hotswap_sdr) - 5;	//Record length (sizeof(sdr) - sizeof(header)) where header size is 5 bytes
	
	amc_hotswap_sdr[5] = owner_id;						//Sensor owner ID
	amc_hotswap_sdr[6] = 0x70;							//Sensor owner LUN (only LUN 0x00, located on IPMB-L bus (0x07) is accepted by the CERN MMC)
	
	amc_hotswap_sdr[7] = HOT_SWAP_SENSOR;				//Sensor number
	
	amc_hotswap_sdr[8] = entity_id;						//Entity ID
	amc_hotswap_sdr[9] = entity_inst;					//Entity instance
	
	return 1;
}

unsigned char set_hotswap_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr){
	//Nothing to do	
	return 0;
}