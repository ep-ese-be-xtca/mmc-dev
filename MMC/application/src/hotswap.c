/*! \file hotswap.c \  Handle switch management */
//*****************************************************************************
//
// File Name	: 'hotswap.c'
// Title		: Handle switch management
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************
#include "../../drivers/drivers.h"

#include "../inc/sdr.h"
#include "../inc/ipmi_if.h"
#include "../inc/sensors.h"
#include "../inc/hotswap.h"

#include "../../user/user_code/config.h"

unsigned char handle_switch_value, handle_state_changed = 0;
unsigned char wait_before_checking = 0;	//Soft filter (bounces)

void hotswap_init(){
	
	if(get_handle_switch_state() == HANDLE_SWITCH_CLOSED){
		set_sensor_value(HOT_SWAP_SENSOR, 0x01);
		sendHotswapEvent(HANDLE_SWITCH_CLOSED);
	}else{
		set_sensor_value(HOT_SWAP_SENSOR, 0x02);
		sendHotswapEvent(HANDLE_SWITCH_OPENED);
	}
	
}

unsigned char event_sent_error = 0;
unsigned char old_state = -1;

void manage_hotswap(){
		
	unsigned char state = get_handle_switch_state();
	if(state != old_state){
		old_state = state;
		set_sensor_value_changed(HOT_SWAP_SENSOR);	
		//printf("Hotswap: state changed (0x%02x) \n\r", state);
	}
	
	if(get_sensor_value_changed_flag(HOT_SWAP_SENSOR) || event_sent_error){
		if(state == HANDLE_SWITCH_CLOSED){			
			set_sensor_value(HOT_SWAP_SENSOR, get_sensor_value(HOT_SWAP_SENSOR) & 0xFD);
			set_sensor_value(HOT_SWAP_SENSOR, get_sensor_value(HOT_SWAP_SENSOR) | 0x01);
			sendHotswapEvent(HANDLE_SWITCH_CLOSED);
		}else{
			set_sensor_value(HOT_SWAP_SENSOR, get_sensor_value(HOT_SWAP_SENSOR) & 0xFE);
			set_sensor_value(HOT_SWAP_SENSOR, get_sensor_value(HOT_SWAP_SENSOR) | 0x02);
			sendHotswapEvent(HANDLE_SWITCH_OPENED);
		}
	}
}

unsigned char sendHotswapEvent(unsigned char value){	
	event_sent_error = ipmi_event_send(HOT_SWAP_SENSOR, ASSERTION_EVENT, &value, 1);
	return 	event_sent_error;
}

unsigned char get_handle_switch_state(){
	#ifndef FORCE_HANDLE_VALUE
		return get_signal(LOCAL_HANDLE_SWITCH)? HANDLE_SWITCH_OPENED:HANDLE_SWITCH_CLOSED;
	#else
		return FORCE_HANDLE_VALUE;
	#endif
}