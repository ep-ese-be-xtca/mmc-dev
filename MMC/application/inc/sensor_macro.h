/*! \file sensor_macro.h \ Definition of sensor macro used for SDR calcul - Header file */
//*****************************************************************************
//
// File Name	: 'sensor_macro.h'
// Title		: Definition of sensor macro used for SDR calcul - Header file
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************

#ifndef SENSOR_MACRO_H_
#define SENSOR_MACRO_H_

	//** FOR AMCs **//
	#if defined(AMC40_RECORD)
		#define AMC_SENSOR_CNT	43
	#elif defined(AMC39_RECORD)
		#define AMC_SENSOR_CNT	42
	#elif defined(AMC38_RECORD)
		#define AMC_SENSOR_CNT	41
	#elif defined(AMC37_RECORD)
		#define AMC_SENSOR_CNT	40
	#elif defined(AMC36_RECORD)
		#define AMC_SENSOR_CNT	39
	#elif defined(AMC35_RECORD)
		#define AMC_SENSOR_CNT	38
	#elif defined(AMC34_RECORD)
		#define AMC_SENSOR_CNT	37
	#elif defined(AMC33_RECORD)
		#define AMC_SENSOR_CNT	36
	#elif defined(AMC32_RECORD)
		#define AMC_SENSOR_CNT	35
	#elif defined(AMC31_RECORD)
		#define AMC_SENSOR_CNT	34
	#elif defined(AMC30_RECORD)
		#define AMC_SENSOR_CNT	33
	#elif defined(AMC29_RECORD)
		#define AMC_SENSOR_CNT	32
	#elif defined(AMC28_RECORD)
		#define AMC_SENSOR_CNT	31
	#elif defined(AMC27_RECORD)
		#define AMC_SENSOR_CNT	30
	#elif defined(AMC26_RECORD)
		#define AMC_SENSOR_CNT	29
	#elif defined(AMC25_RECORD)
		#define AMC_SENSOR_CNT	28
	#elif defined(AMC24_RECORD)
		#define AMC_SENSOR_CNT	27
	#elif defined(AMC23_RECORD)
		#define AMC_SENSOR_CNT	26
	#elif defined(AMC22_RECORD)
		#define AMC_SENSOR_CNT	25
	#elif defined(AMC21_RECORD)
		#define AMC_SENSOR_CNT	24
	#elif defined(AMC20_RECORD)
		#define AMC_SENSOR_CNT	23
	#elif defined(AMC19_RECORD)
		#define AMC_SENSOR_CNT	22
	#elif defined(AMC18_RECORD)
		#define AMC_SENSOR_CNT	21
	#elif defined(AMC17_RECORD)
		#define AMC_SENSOR_CNT	20
	#elif defined(AMC16_RECORD)
		#define AMC_SENSOR_CNT	19
	#elif defined(AMC15_RECORD)
		#define AMC_SENSOR_CNT	18
	#elif defined(AMC14_RECORD)
		#define AMC_SENSOR_CNT	17
	#elif defined(AMC13_RECORD)
		#define AMC_SENSOR_CNT	16
	#elif defined(AMC12_RECORD)
		#define AMC_SENSOR_CNT	15
	#elif defined(AMC11_RECORD)
		#define AMC_SENSOR_CNT	14
	#elif defined(AMC10_RECORD)
		#define AMC_SENSOR_CNT	13
	#elif defined(AMC9_RECORD)
		#define AMC_SENSOR_CNT	12
	#elif defined(AMC8_RECORD)
		#define AMC_SENSOR_CNT	11
	#elif defined(AMC7_RECORD)
		#define AMC_SENSOR_CNT	10
	#elif defined(AMC6_RECORD)
		#define AMC_SENSOR_CNT	9
	#elif defined(AMC5_RECORD)
		#define AMC_SENSOR_CNT	8
	#elif defined(AMC4_RECORD)
		#define AMC_SENSOR_CNT	7
	#elif defined(AMC3_RECORD)
		#define AMC_SENSOR_CNT	6
	#elif defined(AMC2_RECORD)
		#define AMC_SENSOR_CNT	5
	#elif defined(AMC1_RECORD)
		#define AMC_SENSOR_CNT	4
	#elif defined(AMC0_RECORD)
		#define AMC_SENSOR_CNT	3
	#else
		#define AMC_SENSOR_CNT	2
	#endif
	
	#define SENSOR_DECLARATION_LOOP(n)		SENSOR_DECLARATION_LOOP ## n
	//#define SENSOR_DECLARATION_LOOP0		signed char amc_SDR0[] = GENERIC0_RECORD;								//Generated in sdr_init
	#define SENSOR_DECLARATION_LOOP1		signed char amc_SDR1[] = GENERIC1_RECORD;	/*SENSOR_DECLARATION_LOOP0*/
	#define SENSOR_DECLARATION_LOOP2		signed char amc_SDR2[] = GENERIC2_RECORD;	SENSOR_DECLARATION_LOOP1
	#define SENSOR_DECLARATION_LOOP3		signed char amc_SDR3[] = AMC0_RECORD;		SENSOR_DECLARATION_LOOP2
	#define SENSOR_DECLARATION_LOOP4		signed char amc_SDR4[] = AMC1_RECORD;		SENSOR_DECLARATION_LOOP3
	#define SENSOR_DECLARATION_LOOP5		signed char amc_SDR5[] = AMC2_RECORD;		SENSOR_DECLARATION_LOOP4
	#define SENSOR_DECLARATION_LOOP6		signed char amc_SDR6[] = AMC3_RECORD;		SENSOR_DECLARATION_LOOP5
	#define SENSOR_DECLARATION_LOOP7		signed char amc_SDR7[] = AMC4_RECORD;		SENSOR_DECLARATION_LOOP6
	#define SENSOR_DECLARATION_LOOP8		signed char amc_SDR8[] = AMC5_RECORD;		SENSOR_DECLARATION_LOOP7
	#define SENSOR_DECLARATION_LOOP9		signed char amc_SDR9[] = AMC6_RECORD;		SENSOR_DECLARATION_LOOP8
	#define SENSOR_DECLARATION_LOOP10		signed char amc_SDR10[] = AMC7_RECORD;		SENSOR_DECLARATION_LOOP9
	#define SENSOR_DECLARATION_LOOP11		signed char amc_SDR11[] = AMC8_RECORD;		SENSOR_DECLARATION_LOOP10
	#define SENSOR_DECLARATION_LOOP12		signed char amc_SDR12[] = AMC9_RECORD;		SENSOR_DECLARATION_LOOP11
	#define SENSOR_DECLARATION_LOOP13		signed char amc_SDR13[] = AMC10_RECORD;		SENSOR_DECLARATION_LOOP12
	#define SENSOR_DECLARATION_LOOP14		signed char amc_SDR14[] = AMC11_RECORD;		SENSOR_DECLARATION_LOOP13
	#define SENSOR_DECLARATION_LOOP15		signed char amc_SDR15[] = AMC12_RECORD;		SENSOR_DECLARATION_LOOP14
	#define SENSOR_DECLARATION_LOOP16		signed char amc_SDR16[] = AMC13_RECORD;		SENSOR_DECLARATION_LOOP15
	#define SENSOR_DECLARATION_LOOP17		signed char amc_SDR17[] = AMC14_RECORD;		SENSOR_DECLARATION_LOOP16
	#define SENSOR_DECLARATION_LOOP18		signed char amc_SDR18[] = AMC15_RECORD;		SENSOR_DECLARATION_LOOP17
	#define SENSOR_DECLARATION_LOOP19		signed char amc_SDR19[] = AMC16_RECORD;		SENSOR_DECLARATION_LOOP18
	#define SENSOR_DECLARATION_LOOP20		signed char amc_SDR20[] = AMC17_RECORD;		SENSOR_DECLARATION_LOOP19
	#define SENSOR_DECLARATION_LOOP21		signed char amc_SDR21[] = AMC18_RECORD;		SENSOR_DECLARATION_LOOP20
	#define SENSOR_DECLARATION_LOOP22		signed char amc_SDR22[] = AMC19_RECORD;		SENSOR_DECLARATION_LOOP21
	#define SENSOR_DECLARATION_LOOP23		signed char amc_SDR23[] = AMC20_RECORD;		SENSOR_DECLARATION_LOOP22
	#define SENSOR_DECLARATION_LOOP24		signed char amc_SDR24[] = AMC21_RECORD;		SENSOR_DECLARATION_LOOP23
	#define SENSOR_DECLARATION_LOOP25		signed char amc_SDR25[] = AMC22_RECORD;		SENSOR_DECLARATION_LOOP24
	#define SENSOR_DECLARATION_LOOP26		signed char amc_SDR26[] = AMC23_RECORD;		SENSOR_DECLARATION_LOOP25
	#define SENSOR_DECLARATION_LOOP27		signed char amc_SDR27[] = AMC24_RECORD;		SENSOR_DECLARATION_LOOP26
	#define SENSOR_DECLARATION_LOOP28		signed char amc_SDR28[] = AMC25_RECORD;		SENSOR_DECLARATION_LOOP27
	#define SENSOR_DECLARATION_LOOP29		signed char amc_SDR29[] = AMC26_RECORD;		SENSOR_DECLARATION_LOOP28
	#define SENSOR_DECLARATION_LOOP30		signed char amc_SDR30[] = AMC27_RECORD;		SENSOR_DECLARATION_LOOP29
	#define SENSOR_DECLARATION_LOOP31		signed char amc_SDR31[] = AMC28_RECORD;		SENSOR_DECLARATION_LOOP30
	#define SENSOR_DECLARATION_LOOP32		signed char amc_SDR32[] = AMC29_RECORD;		SENSOR_DECLARATION_LOOP31
	#define SENSOR_DECLARATION_LOOP33		signed char amc_SDR33[] = AMC30_RECORD;		SENSOR_DECLARATION_LOOP32
	#define SENSOR_DECLARATION_LOOP34		signed char amc_SDR34[] = AMC31_RECORD;		SENSOR_DECLARATION_LOOP33
	#define SENSOR_DECLARATION_LOOP35		signed char amc_SDR35[] = AMC32_RECORD;		SENSOR_DECLARATION_LOOP34
	#define SENSOR_DECLARATION_LOOP36		signed char amc_SDR36[] = AMC33_RECORD;		SENSOR_DECLARATION_LOOP35
	#define SENSOR_DECLARATION_LOOP37		signed char amc_SDR37[] = AMC34_RECORD;		SENSOR_DECLARATION_LOOP36
	#define SENSOR_DECLARATION_LOOP38		signed char amc_SDR38[] = AMC35_RECORD;		SENSOR_DECLARATION_LOOP37
	#define SENSOR_DECLARATION_LOOP39		signed char amc_SDR39[] = AMC36_RECORD;		SENSOR_DECLARATION_LOOP38
	#define SENSOR_DECLARATION_LOOP40		signed char amc_SDR40[] = AMC37_RECORD;		SENSOR_DECLARATION_LOOP39
	#define SENSOR_DECLARATION_LOOP41		signed char amc_SDR41[] = AMC38_RECORD;		SENSOR_DECLARATION_LOOP40
	#define SENSOR_DECLARATION_LOOP42		signed char amc_SDR42[] = AMC39_RECORD;		SENSOR_DECLARATION_LOOP41
	#define SENSOR_DECLARATION_LOOP43		signed char amc_SDR43[] = AMC40_RECORD;		SENSOR_DECLARATION_LOOP42

	#define SENSOR_INIT_LOOP(n)				SENSOR_INIT_LOOP ## n
	#define SENSOR_INIT_LOOP0				amc_sdrPtr[0] = amc_SDR0;	amc_sdrLen[0] = sizeof(amc_SDR0);
	#define SENSOR_INIT_LOOP1				amc_sdrPtr[1] = amc_SDR1;	amc_sdrLen[1] = sizeof(amc_SDR1);	SENSOR_INIT_LOOP0
	#define SENSOR_INIT_LOOP2				amc_sdrPtr[2] = amc_SDR2;	amc_sdrLen[2] = sizeof(amc_SDR2);	SENSOR_INIT_LOOP1
	#define SENSOR_INIT_LOOP3				amc_sdrPtr[3] = amc_SDR3;	amc_sdrLen[3] = sizeof(amc_SDR3);	SENSOR_INIT_LOOP2
	#define SENSOR_INIT_LOOP4				amc_sdrPtr[4] = amc_SDR4;	amc_sdrLen[4] = sizeof(amc_SDR4);	SENSOR_INIT_LOOP3
	#define SENSOR_INIT_LOOP5				amc_sdrPtr[5] = amc_SDR5;	amc_sdrLen[5] = sizeof(amc_SDR5);	SENSOR_INIT_LOOP4
	#define SENSOR_INIT_LOOP6				amc_sdrPtr[6] = amc_SDR6;	amc_sdrLen[6] = sizeof(amc_SDR6);	SENSOR_INIT_LOOP5
	#define SENSOR_INIT_LOOP7				amc_sdrPtr[7] = amc_SDR7;	amc_sdrLen[7] = sizeof(amc_SDR7);	SENSOR_INIT_LOOP6
	#define SENSOR_INIT_LOOP8				amc_sdrPtr[8] = amc_SDR8;	amc_sdrLen[8] = sizeof(amc_SDR8);	SENSOR_INIT_LOOP7
	#define SENSOR_INIT_LOOP9				amc_sdrPtr[9] = amc_SDR9;	amc_sdrLen[9] = sizeof(amc_SDR9);	SENSOR_INIT_LOOP8
	#define SENSOR_INIT_LOOP10				amc_sdrPtr[10] = amc_SDR10;	amc_sdrLen[10] = sizeof(amc_SDR10);	SENSOR_INIT_LOOP9
	#define SENSOR_INIT_LOOP11				amc_sdrPtr[11] = amc_SDR11;	amc_sdrLen[11] = sizeof(amc_SDR11);	SENSOR_INIT_LOOP10
	#define SENSOR_INIT_LOOP12				amc_sdrPtr[12] = amc_SDR12;	amc_sdrLen[12] = sizeof(amc_SDR12);	SENSOR_INIT_LOOP11
	#define SENSOR_INIT_LOOP13				amc_sdrPtr[13] = amc_SDR13;	amc_sdrLen[13] = sizeof(amc_SDR13);	SENSOR_INIT_LOOP12
	#define SENSOR_INIT_LOOP14				amc_sdrPtr[14] = amc_SDR14;	amc_sdrLen[14] = sizeof(amc_SDR14);	SENSOR_INIT_LOOP13
	#define SENSOR_INIT_LOOP15				amc_sdrPtr[15] = amc_SDR15;	amc_sdrLen[15] = sizeof(amc_SDR15);	SENSOR_INIT_LOOP14
	#define SENSOR_INIT_LOOP16				amc_sdrPtr[16] = amc_SDR16;	amc_sdrLen[16] = sizeof(amc_SDR16);	SENSOR_INIT_LOOP15
	#define SENSOR_INIT_LOOP17				amc_sdrPtr[17] = amc_SDR17;	amc_sdrLen[17] = sizeof(amc_SDR17);	SENSOR_INIT_LOOP16
	#define SENSOR_INIT_LOOP18				amc_sdrPtr[18] = amc_SDR18;	amc_sdrLen[18] = sizeof(amc_SDR18);	SENSOR_INIT_LOOP17
	#define SENSOR_INIT_LOOP19				amc_sdrPtr[19] = amc_SDR19;	amc_sdrLen[19] = sizeof(amc_SDR19);	SENSOR_INIT_LOOP18
	#define SENSOR_INIT_LOOP20				amc_sdrPtr[20] = amc_SDR20;	amc_sdrLen[20] = sizeof(amc_SDR20);	SENSOR_INIT_LOOP19
	#define SENSOR_INIT_LOOP21				amc_sdrPtr[21] = amc_SDR21;	amc_sdrLen[21] = sizeof(amc_SDR21);	SENSOR_INIT_LOOP20
	#define SENSOR_INIT_LOOP22				amc_sdrPtr[22] = amc_SDR22;	amc_sdrLen[22] = sizeof(amc_SDR22);	SENSOR_INIT_LOOP21
	#define SENSOR_INIT_LOOP23				amc_sdrPtr[23] = amc_SDR23;	amc_sdrLen[23] = sizeof(amc_SDR23);	SENSOR_INIT_LOOP22
	#define SENSOR_INIT_LOOP24				amc_sdrPtr[24] = amc_SDR24;	amc_sdrLen[24] = sizeof(amc_SDR24);	SENSOR_INIT_LOOP23
	#define SENSOR_INIT_LOOP25				amc_sdrPtr[25] = amc_SDR25;	amc_sdrLen[25] = sizeof(amc_SDR25);	SENSOR_INIT_LOOP24
	#define SENSOR_INIT_LOOP26				amc_sdrPtr[26] = amc_SDR26;	amc_sdrLen[26] = sizeof(amc_SDR26);	SENSOR_INIT_LOOP25
	#define SENSOR_INIT_LOOP27				amc_sdrPtr[27] = amc_SDR27;	amc_sdrLen[27] = sizeof(amc_SDR27);	SENSOR_INIT_LOOP26
	#define SENSOR_INIT_LOOP28				amc_sdrPtr[28] = amc_SDR28;	amc_sdrLen[28] = sizeof(amc_SDR28);	SENSOR_INIT_LOOP27
	#define SENSOR_INIT_LOOP29				amc_sdrPtr[29] = amc_SDR29;	amc_sdrLen[29] = sizeof(amc_SDR29);	SENSOR_INIT_LOOP28
	#define SENSOR_INIT_LOOP30				amc_sdrPtr[30] = amc_SDR30;	amc_sdrLen[30] = sizeof(amc_SDR30);	SENSOR_INIT_LOOP29
	#define SENSOR_INIT_LOOP31				amc_sdrPtr[31] = amc_SDR31;	amc_sdrLen[31] = sizeof(amc_SDR31);	SENSOR_INIT_LOOP30
	#define SENSOR_INIT_LOOP32				amc_sdrPtr[32] = amc_SDR32;	amc_sdrLen[32] = sizeof(amc_SDR32);	SENSOR_INIT_LOOP31
	#define SENSOR_INIT_LOOP33				amc_sdrPtr[33] = amc_SDR33;	amc_sdrLen[33] = sizeof(amc_SDR33);	SENSOR_INIT_LOOP32
	#define SENSOR_INIT_LOOP34				amc_sdrPtr[34] = amc_SDR34;	amc_sdrLen[34] = sizeof(amc_SDR34);	SENSOR_INIT_LOOP33
	#define SENSOR_INIT_LOOP35				amc_sdrPtr[35] = amc_SDR35;	amc_sdrLen[35] = sizeof(amc_SDR35);	SENSOR_INIT_LOOP34
	#define SENSOR_INIT_LOOP36				amc_sdrPtr[36] = amc_SDR36;	amc_sdrLen[36] = sizeof(amc_SDR36);	SENSOR_INIT_LOOP35
	#define SENSOR_INIT_LOOP37				amc_sdrPtr[37] = amc_SDR37;	amc_sdrLen[37] = sizeof(amc_SDR37);	SENSOR_INIT_LOOP36
	#define SENSOR_INIT_LOOP38				amc_sdrPtr[38] = amc_SDR38;	amc_sdrLen[38] = sizeof(amc_SDR38);	SENSOR_INIT_LOOP37
	#define SENSOR_INIT_LOOP39				amc_sdrPtr[39] = amc_SDR29;	amc_sdrLen[39] = sizeof(amc_SDR39);	SENSOR_INIT_LOOP38
	#define SENSOR_INIT_LOOP40				amc_sdrPtr[40] = amc_SDR40;	amc_sdrLen[40] = sizeof(amc_SDR40);	SENSOR_INIT_LOOP39
	#define SENSOR_INIT_LOOP41				amc_sdrPtr[41] = amc_SDR41;	amc_sdrLen[41] = sizeof(amc_SDR41);	SENSOR_INIT_LOOP40
	#define SENSOR_INIT_LOOP42				amc_sdrPtr[42] = amc_SDR42;	amc_sdrLen[42] = sizeof(amc_SDR42);	SENSOR_INIT_LOOP41
	#define SENSOR_INIT_LOOP43				amc_sdrPtr[43] = amc_SDR43;	amc_sdrLen[43] = sizeof(amc_SDR43);	SENSOR_INIT_LOOP42

	#define AMC_SDR_DECLARATION(n)				SENSOR_DECLARATION_LOOP(n)
	#define AMC_SDR_INIT(n)						SENSOR_INIT_LOOP(n)

	//** FOR RTMs **//
	#if defined(RTM18_RECORD)
		#define RTM_SENSOR_CNT	20
	#elif defined(RTM17_RECORD)
		#define RTM_SENSOR_CNT	19
	#elif defined(RTM16_RECORD)
		#define RTM_SENSOR_CNT	18
	#elif defined(RTM15_RECORD)
		#define RTM_SENSOR_CNT	17
	#elif defined(RTM14_RECORD)
		#define RTM_SENSOR_CNT	16
	#elif defined(RTM13_RECORD)
		#define RTM_SENSOR_CNT	15
	#elif defined(RTM12_RECORD)
		#define RTM_SENSOR_CNT	14
	#elif defined(RTM11_RECORD)
		#define RTM_SENSOR_CNT	13
	#elif defined(RTM10_RECORD)
		#define RTM_SENSOR_CNT	12
	#elif defined(RTM9_RECORD)
		#define RTM_SENSOR_CNT	11
	#elif defined(RTM8_RECORD)
		#define RTM_SENSOR_CNT	10
	#elif defined(RTM7_RECORD)
		#define RTM_SENSOR_CNT	9
	#elif defined(RTM6_RECORD)
		#define RTM_SENSOR_CNT	8
	#elif defined(RTM5_RECORD)
		#define RTM_SENSOR_CNT	7
	#elif defined(RTM4_RECORD)
		#define RTM_SENSOR_CNT	6
	#elif defined(RTM3_RECORD)
		#define RTM_SENSOR_CNT	5
	#elif defined(RTM2_RECORD)
		#define RTM_SENSOR_CNT	4
	#elif defined(RTM1_RECORD)
		#define RTM_SENSOR_CNT	3
	#elif defined(RTM0_RECORD)
		#define RTM_SENSOR_CNT	2
	#else
		#define RTM_SENSOR_CNT	1
	#endif
	
	#define RTM_SENSOR_DECLARATION_LOOP(n)		RTM_SENSOR_DECLARATION_LOOP ## n
	#define RTM_SENSOR_DECLARATION_LOOP0		signed char rtm_SDR0[] = RTM_GENERIC0_RECORD;								//Generated in sdr_init
	#define RTM_SENSOR_DECLARATION_LOOP1		signed char rtm_SDR1[] = RTM_GENERIC1_RECORD;	/*RTM_SENSOR_DECLARATION_LOOP0*/
	#define RTM_SENSOR_DECLARATION_LOOP2		signed char rtm_SDR2[] = RTM0_RECORD;			RTM_SENSOR_DECLARATION_LOOP1
	#define RTM_SENSOR_DECLARATION_LOOP3		signed char rtm_SDR3[] = RTM1_RECORD;			RTM_SENSOR_DECLARATION_LOOP2
	#define RTM_SENSOR_DECLARATION_LOOP4		signed char rtm_SDR4[] = RTM2_RECORD;			RTM_SENSOR_DECLARATION_LOOP3
	#define RTM_SENSOR_DECLARATION_LOOP5		signed char rtm_SDR5[] = RTM3_RECORD;			RTM_SENSOR_DECLARATION_LOOP4
	#define RTM_SENSOR_DECLARATION_LOOP6		signed char rtm_SDR6[] = RTM4_RECORD;			RTM_SENSOR_DECLARATION_LOOP5
	#define RTM_SENSOR_DECLARATION_LOOP7		signed char rtm_SDR7[] = RTM5_RECORD;			RTM_SENSOR_DECLARATION_LOOP6
	#define RTM_SENSOR_DECLARATION_LOOP8		signed char rtm_SDR8[] = RTM6_RECORD;			RTM_SENSOR_DECLARATION_LOOP7
	#define RTM_SENSOR_DECLARATION_LOOP9		signed char rtm_SDR9[] = RTM7_RECORD;			RTM_SENSOR_DECLARATION_LOOP8
	#define RTM_SENSOR_DECLARATION_LOOP10		signed char rtm_SDR10[] = RTM8_RECORD;			RTM_SENSOR_DECLARATION_LOOP9
	#define RTM_SENSOR_DECLARATION_LOOP11		signed char rtm_SDR11[] = RTM9_RECORD;			RTM_SENSOR_DECLARATION_LOOP10
	#define RTM_SENSOR_DECLARATION_LOOP12		signed char rtm_SDR12[] = RTM10_RECORD;			RTM_SENSOR_DECLARATION_LOOP11
	#define RTM_SENSOR_DECLARATION_LOOP13		signed char rtm_SDR13[] = RTM11_RECORD;			RTM_SENSOR_DECLARATION_LOOP12
	#define RTM_SENSOR_DECLARATION_LOOP14		signed char rtm_SDR14[] = RTM12_RECORD;			RTM_SENSOR_DECLARATION_LOOP13
	#define RTM_SENSOR_DECLARATION_LOOP15		signed char rtm_SDR15[] = RTM13_RECORD;			RTM_SENSOR_DECLARATION_LOOP14
	#define RTM_SENSOR_DECLARATION_LOOP16		signed char rtm_SDR16[] = RTM14_RECORD;			RTM_SENSOR_DECLARATION_LOOP15
	#define RTM_SENSOR_DECLARATION_LOOP17		signed char rtm_SDR17[] = RTM15_RECORD;			RTM_SENSOR_DECLARATION_LOOP16
	#define RTM_SENSOR_DECLARATION_LOOP18		signed char rtm_SDR18[] = RTM16_RECORD;			RTM_SENSOR_DECLARATION_LOOP17
	#define RTM_SENSOR_DECLARATION_LOOP19		signed char rtm_SDR19[] = RTM17_RECORD;			RTM_SENSOR_DECLARATION_LOOP18
	#define RTM_SENSOR_DECLARATION_LOOP20		signed char rtm_SDR20[] = RTM18_RECORD;			RTM_SENSOR_DECLARATION_LOOP19

	#define RTM_SENSOR_INIT_LOOP(n)				RTM_SENSOR_INIT_LOOP ## n
	#define RTM_SENSOR_INIT_LOOP0				rtm_sdrPtr[0] = rtm_SDR0;	rtm_sdrLen[0] = sizeof(rtm_SDR0);
	#define RTM_SENSOR_INIT_LOOP1				rtm_sdrPtr[1] = rtm_SDR1;	rtm_sdrLen[1] = sizeof(rtm_SDR1);	RTM_SENSOR_INIT_LOOP0
	#define RTM_SENSOR_INIT_LOOP2				rtm_sdrPtr[2] = rtm_SDR2;	rtm_sdrLen[2] = sizeof(rtm_SDR2);	RTM_SENSOR_INIT_LOOP1
	#define RTM_SENSOR_INIT_LOOP3				rtm_sdrPtr[3] = rtm_SDR3;	rtm_sdrLen[3] = sizeof(rtm_SDR3);	RTM_SENSOR_INIT_LOOP2
	#define RTM_SENSOR_INIT_LOOP4				rtm_sdrPtr[4] = rtm_SDR4;	rtm_sdrLen[4] = sizeof(rtm_SDR4);	RTM_SENSOR_INIT_LOOP3
	#define RTM_SENSOR_INIT_LOOP5				rtm_sdrPtr[5] = rtm_SDR5;	rtm_sdrLen[5] = sizeof(rtm_SDR5);	RTM_SENSOR_INIT_LOOP4
	#define RTM_SENSOR_INIT_LOOP6				rtm_sdrPtr[6] = rtm_SDR6;	rtm_sdrLen[6] = sizeof(rtm_SDR6);	RTM_SENSOR_INIT_LOOP5
	#define RTM_SENSOR_INIT_LOOP7				rtm_sdrPtr[7] = rtm_SDR7;	rtm_sdrLen[7] = sizeof(rtm_SDR7);	RTM_SENSOR_INIT_LOOP6
	#define RTM_SENSOR_INIT_LOOP8				rtm_sdrPtr[8] = rtm_SDR8;	rtm_sdrLen[8] = sizeof(rtm_SDR8);	RTM_SENSOR_INIT_LOOP7
	#define RTM_SENSOR_INIT_LOOP9				rtm_sdrPtr[9] = rtm_SDR9;	rtm_sdrLen[9] = sizeof(rtm_SDR9);	RTM_SENSOR_INIT_LOOP8
	#define RTM_SENSOR_INIT_LOOP10				rtm_sdrPtr[10] = rtm_SDR10;	rtm_sdrLen[10] = sizeof(rtm_SDR10);	RTM_SENSOR_INIT_LOOP9
	#define RTM_SENSOR_INIT_LOOP11				rtm_sdrPtr[11] = rtm_SDR11;	rtm_sdrLen[11] = sizeof(rtm_SDR11);	RTM_SENSOR_INIT_LOOP10
	#define RTM_SENSOR_INIT_LOOP12				rtm_sdrPtr[12] = rtm_SDR12;	rtm_sdrLen[12] = sizeof(rtm_SDR12);	RTM_SENSOR_INIT_LOOP11
	#define RTM_SENSOR_INIT_LOOP13				rtm_sdrPtr[13] = rtm_SDR13;	rtm_sdrLen[13] = sizeof(rtm_SDR13);	RTM_SENSOR_INIT_LOOP12
	#define RTM_SENSOR_INIT_LOOP14				rtm_sdrPtr[14] = rtm_SDR14;	rtm_sdrLen[14] = sizeof(rtm_SDR14);	RTM_SENSOR_INIT_LOOP13
	#define RTM_SENSOR_INIT_LOOP15				rtm_sdrPtr[15] = rtm_SDR15;	rtm_sdrLen[15] = sizeof(rtm_SDR15);	RTM_SENSOR_INIT_LOOP14
	#define RTM_SENSOR_INIT_LOOP16				rtm_sdrPtr[16] = rtm_SDR16;	rtm_sdrLen[16] = sizeof(rtm_SDR16);	RTM_SENSOR_INIT_LOOP15
	#define RTM_SENSOR_INIT_LOOP17				rtm_sdrPtr[17] = rtm_SDR17;	rtm_sdrLen[17] = sizeof(rtm_SDR17);	RTM_SENSOR_INIT_LOOP16
	#define RTM_SENSOR_INIT_LOOP18				rtm_sdrPtr[18] = rtm_SDR18;	rtm_sdrLen[18] = sizeof(rtm_SDR18);	RTM_SENSOR_INIT_LOOP17
	#define RTM_SENSOR_INIT_LOOP19				rtm_sdrPtr[19] = rtm_SDR19;	rtm_sdrLen[19] = sizeof(rtm_SDR19);	RTM_SENSOR_INIT_LOOP18
	#define RTM_SENSOR_INIT_LOOP20				rtm_sdrPtr[20] = rtm_SDR20;	rtm_sdrLen[20] = sizeof(rtm_SDR20);	RTM_SENSOR_INIT_LOOP19

	#define RTM_SDR_DECLARATION(n)				RTM_SENSOR_DECLARATION_LOOP(n)
	#define RTM_SDR_INIT(n)						RTM_SENSOR_INIT_LOOP(n)

#endif /* SENSOR_MACRO_H_ */