/*! \file i2rtm.h \brief Software I2C interface using port pins. */
//*****************************************************************************
//
// File Name	: 'i2csw.h'
// Title		: Software I2C interface to RTM module
// Author		: Vahan Petrosyan
// Created		: 10/19/2010
// Target MCU	: Atmel AVR series
//
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
//*****************************************************************************


#ifndef I2RTM_H
#define I2RTM_H

// Constants
#define READ					0x01	// I2C READ bit
#define RTM_EEPROM_ADDR			0x50
#define RTM_IO_PORTS_ADDR		0x26

// Function prototypes
unsigned char  i2crtmInit(void);  // Initialize I2C interface pins
unsigned char  write_rtm_eeprom_page(unsigned short base_addr, unsigned char  length, unsigned char  *data, unsigned short after_write_delay);
unsigned char  rtm_fru_read_byte(unsigned short address);
unsigned char  rtm_fru_data_read(unsigned short address, unsigned char  len, unsigned char * data);

#endif
