/*! \file payload.h \ Payload management - Header file */
//*****************************************************************************
//
// File Name	: 'payload.h'
// Title		: Payload management - Header file
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************

#ifndef PAYLOAD_H_
#define PAYLOAD_H_

	#define FRU_COLD_RESET        0
	#define FRU_WARM_RESET        1
	#define FRU_REBOOT            2
	#define FRU_QUIESCE           4
	#define POWER_UP              5
	#define POWER_DOWN            6

	#define SET_PAYLOAD_SIGNAL(id)								set_signal(id);
	#define CLEAR_PAYLOAD_SIGNAL(id)							clear_signal(id);
	#define SET_REGISTER(reg, val)								reg = val;


	#define CUSTOM_C(c_code)							watchdog_reset(); c_code watchdog_reset(); 
	#define DELAY(ms)									watchdog_reset(); delay_ms(ms);

	#define WAIT_FOR_HIGH(id, timeout, error)																			\
	watchdog_reset(); 																									\
	for(i=0; i < timeout && !(inb(id ## _PPIN) & BV(id ## _PIN)); i++){		watchdog_reset();		delay_ms(1);	}	\
	if(!(inb(id ## _PPIN) & BV(id ## _PIN)))										{ error }

	#define WAIT_FOR_LOW(id, timeout, error)																			\
	watchdog_reset(); 																									\
	for(i=0; i < timeout && (inb(id ## _PPIN) & BV(id ## _PIN)); i++){		watchdog_reset();		delay_ms(1);	}	\
	if((inb(id ## _PPIN) & BV(id ## _PIN)))											{ error }

	#define PON_ERROR																						\
	if(ipmi_is_init()){																				\
		set_sensor_value(HOT_SWAP_SENSOR, get_sensor_value(HOT_SWAP_SENSOR) | 0x08);				\
		sendHotswapEvent(HOTSWAP_POWER_FAULT);														\
	}																								\
	desactive_payload();																			\
	return 1;

	#define POFF_ERROR		return 1;

	#define PON_CONTINUE	asm volatile("nop");

	unsigned char active_payload();
	unsigned char desactive_payload();
	unsigned char ipmi_picmg_fru_control(unsigned char control_type);
	unsigned char ipmi_picmg_get_fru_control_capabilities();

	unsigned char get_deactivation_flag();
	void set_deactivation_flag();
	void manage_payload();
	unsigned char get_fru_state();
	
#endif /* PAYLOAD_H_ */