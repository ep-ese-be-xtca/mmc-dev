/*! \file hotswap.h \ Hotswap management - Header file */
//*****************************************************************************
//
// File Name	: 'hotswap.c'
// Title		: Hotswap management - Header file
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************


#ifndef HOTSWAP_H_
#define HOTSWAP_H_

#define HANDLE_SWITCH_CLOSED		0x00
#define HANDLE_SWITCH_OPENED		0x01
#define HOTSWAP_QUIESCED			0x02
#define HOTSWAP_POWER_FAULT			0x03

void hotswap_init();
void manage_hotswap();
unsigned char sendHotswapEvent(unsigned char  value);
unsigned char  get_handle_switch_state();

#endif /* HOTSWAP_H_ */