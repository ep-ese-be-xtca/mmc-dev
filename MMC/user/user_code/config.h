/*
 * config.h
 *
 * Created: 19/03/2015 18:20:14
 *  Author: jumendez
 */ 


#ifndef CONFIG_H_
#define CONFIG_H_

/** General configuration */
	unsigned char user_main_callback();
	#define USER_MAIN_CALLBACK	user_main_callback()
	
	#define FORCE_WRITE_FRU
	
/** RTM Configuration */
        //#define USE_RTM

        #define FRU_RTM_EXTENSION               " RTM"
        #define COMPATIBILITY_IDENTIFIER        0x00000000
        #define FORCE_URTM_FRU_WRITE
		
/** ENABLE OEM COMMAND */
        /** No OEM commands for ALB */

/** Product information */
        #define IPMI_MSG_MANU_ID_LSB 0x60   //NOTE: Manufacturer identification is handled by http://www.iana.org/assignments/enterprise-numbers
        #define IPMI_MSG_MANU_ID_B2  0x00       //CERN IANA ID = 0x000060
        #define IPMI_MSG_MANU_ID_MSB 0x00

        #define IPMI_MSG_PROD_ID_LSB 0x35
        #define IPMI_MSG_PROD_ID_MSB 0x12

        #define MMC_FW_REL_MAJ       1                  // major firmware release version ( < 128 )
        #define MMC_FW_REL_MIN       0                  // minor firmware release version ( < 256 )

        #define FRU_NAME                        "LoadBoard"
        
/** USER GPIO/ADC INITIALIZATION */
        /** No GPIO used for ALB */
        /** No custom ADC input for ALB */
        
/** CUSTOM ADDRESSES FOR BENCHTOP */
	#define CUSTOM_ADDR_LIST									\
		ADDR(0xF0, UNCONNECTED, UNCONNECTED, UNCONNECTED)		\
		ADDR(0xFF, POWERED, POWERED, POWERED)

/** CUSTOM ADDRESSES FOR BENCHTOP */
	#define iRTM_ADDR_LIST										\
		ADDR(0xEA, POWERED, POWERED, GROUNDED)

/** PAYLOD CONFIGURATION */
        //Define power on sequence (Mandatory)
		#ifdef AT32UC3A
			#define POWER_ON_SEQ                                                                                            \
				SET_PAYLOAD_SIGNAL(PWR_GPIO_ENABLE)  

			//Define power OFF sequence (Mandatory)
			#define POWER_OFF_SEQ                                                                                           \
				CLEAR_PAYLOAD_SIGNAL(PWR_GPIO_ENABLE) 
		
			//Define reboot sequence (Optional)
			#define REBOOT_SEQ                                                                                                      \
				CLEAR_PAYLOAD_SIGNAL(PWR_GPIO_ENABLE)                                                  \
				DELAY(500)                                                                                                              \
				SET_PAYLOAD_SIGNAL(PWR_GPIO_ENABLE)   
			
		#endif
		#ifdef ATMEGA128
			#define POWER_ON_SEQ                                                                                            \
					SET_PAYLOAD_SIGNAL(LOCAL_DCDC_ENABLE)                                                   \
					SET_PAYLOAD_SIGNAL(LOCAL_REG_ENABLE)                                                    

			//Define power OFF sequence (Mandatory) 
			#define POWER_OFF_SEQ                                                                                           \
					CLEAR_PAYLOAD_SIGNAL(LOCAL_DCDC_ENABLE)                                                 \
					CLEAR_PAYLOAD_SIGNAL(LOCAL_REG_ENABLE)
        
			//Define reboot sequence (Optional)
			#define REBOOT_SEQ                                                                                                      \
					CLEAR_PAYLOAD_SIGNAL(LOCAL_DCDC_ENABLE)                                                 \
					CLEAR_PAYLOAD_SIGNAL(LOCAL_REG_ENABLE)                                                  \
					DELAY(500)                                                                                                              \
					SET_PAYLOAD_SIGNAL(LOCAL_DCDC_ENABLE)                                                   \
					SET_PAYLOAD_SIGNAL(LOCAL_REG_ENABLE)
        #endif
		
        //Define warm reset sequence (Optional) - Set AMC + RTM power to 0
        #define WARM_RESET_SEQ                                                                                          \
                CUSTOM_C(                                                                                                               \
                        reset_led(0, 3);                                                                                        \
                        reset_led(0, 4);                                                                                        \
                        reset_led(0, 5);                                                                                        \
                        reset_led(0, 6);                                                                                        \
                        reset_led(0, 7);                                                                                        \
                        reset_led(0, 8);                                                                                        \
                        reset_led(0, 9);                                                                                        \
                        reset_led(0, 10);                                                                                       \
                        reset_led(1, 3);                                                                                        \
                        reset_led(1, 4);                                                                                        \
                        reset_led(1, 5);                                                                                        \
                        reset_led(1, 6);                                                                                        \
                        reset_led(1, 7);                                                                                        \
                        reset_led(1, 8);                                                                                        \
                        reset_led(1, 9);                                                                                        \
                )

        //Define cold reset sequence (Optional) 
        /** No cold reset for ALB */


/** LED CONFIGURATION */
        #define AMC_USER_LED_CNT        8
        #define AMC_USER_LED_LIST                                                                       \
                {                                                                                                               \
                        {       /* LED ID 3 */                                                                  \
                                io_type: IO_EXTENDER_PCF8574AT,                                 \
                                addr:0x3A,                                                                              \
                                pin:1,                                                                                  \
                                colour: GREEN,                                                                  \
                                init: INACTIVE,                                                                 \
                                active: LOW,                                                                    \
                                inactive: HIGH                                                                  \
                        },                                                                                                      \
                        {       /* LED ID 4     */                                                                      \
                                io_type: IO_EXTENDER_PCF8574AT,                                 \
                                addr:0x3A,                                                                              \
                                pin:2,                                                                                  \
                                colour: GREEN,                                                                  \
                                init: INACTIVE,                                                                 \
                                active: LOW,                                                                    \
                                inactive: HIGH                                                                  \
                        },                                                                                                      \
                        {       /* LED ID 5             */                                                              \
                                io_type: IO_EXTENDER_PCF8574AT,                                 \
                                addr:0x3A,                                                                              \
                                pin:3,                                                                                  \
                                colour: GREEN,                                                                  \
                                init: INACTIVE,                                                                 \
                                active: LOW,                                                                    \
                                inactive: HIGH                                                                  \
                        },                                                                                                      \
                        {       /* LED ID 6             */                                                              \
                                io_type: IO_EXTENDER_PCF8574AT,                                 \
                                addr:0x3A,                                                                              \
                                pin:4,                                                                                  \
                                colour: GREEN,                                                                  \
                                init: INACTIVE,                                                                 \
                                active: LOW,                                                                    \
                                inactive: HIGH                                                                  \
                        },                                                                                                      \
                        {       /* LED ID 7     */                                                                      \
                                io_type: IO_EXTENDER_PCF8574AT,                                 \
                                addr:0x3A,                                                                              \
                                pin:5,                                                                                  \
                                colour: GREEN,                                                                  \
                                init: INACTIVE,                                                                 \
                                active: LOW,                                                                    \
                                inactive: HIGH                                                                  \
                        },                                                                                                      \
                        {       /* LED ID 8     */                                                                      \
                                io_type: IO_EXTENDER_PCF8574AT,                                 \
                                addr:0x3A,                                                                              \
                                pin:6,                                                                                  \
                                colour: GREEN,                                                                  \
                                init: INACTIVE,                                                                 \
                                active: LOW,                                                                    \
                                inactive: HIGH                                                                  \
                        },                                                                                                      \
                        {       /* LED ID 9             */                                                              \
                                io_type: IO_EXTENDER_PCF8574AT,                                 \
                                addr:0x3A,                                                                              \
                                pin:7,                                                                                  \
                                colour: GREEN,                                                                  \
                                init: INACTIVE,                                                                 \
                                active: LOW,                                                                    \
                                inactive: HIGH                                                                  \
                        },                                                                                                      \
                        {       /* LED ID 10    */                                                              \
                                io_type: IO_EXTENDER_PCF8574AT,                                 \
                                addr:0x3A,                                                                              \
                                pin:8,                                                                                  \
                                colour: GREEN,                                                                  \
                                init: INACTIVE,                                                                 \
                                active: LOW,                                                                    \
                                inactive: HIGH                                                                  \
                        }                                                                                                       \
                }


        #define RTM_USER_LED_CNT        7
        #define RTM_USER_LED_LIST                                                                       \
        {                                                                                                               \
                {       /* LED ID 3 */                                                                  \
                        io_type: IO_EXTENDER_PCF8574AT,                                 \
                        addr:0x20,                                                                              \
                        pin:1,                                                                                  \
                        colour: RED,                                                                    \
                        init: INACTIVE,                                                                 \
                        active: LOW,                                                                    \
                        inactive: HIGH                                                                  \
                },                                                                                                      \
                {       /* LED ID 4     */                                                                      \
                        io_type: IO_EXTENDER_PCF8574AT,                                 \
                        addr:0x20,                                                                              \
                        pin:2,                                                                                  \
                        colour: RED,                                                                    \
                        init: INACTIVE,                                                                 \
                        active: LOW,                                                                    \
                        inactive: HIGH                                                                  \
                },                                                                                                      \
                {       /* LED ID 5             */                                                              \
                        io_type: IO_EXTENDER_PCF8574AT,                                 \
                        addr:0x20,                                                                              \
                        pin:3,                                                                                  \
                        colour: RED,                                                                    \
                        init: INACTIVE,                                                                 \
                        active: LOW,                                                                    \
                        inactive: HIGH                                                                  \
                },                                                                                                      \
                {       /* LED ID 6             */                                                              \
                        io_type: IO_EXTENDER_PCF8574AT,                                 \
                        addr:0x20,                                                                              \
                        pin:4,                                                                                  \
                        colour: RED,                                                                    \
                        init: INACTIVE,                                                                 \
                        active: LOW,                                                                    \
                        inactive: HIGH                                                                  \
                },                                                                                                      \
                {       /* LED ID 7     */                                                                      \
                        io_type: IO_EXTENDER_PCF8574AT,                                 \
                        addr:0x20,                                                                              \
                        pin:5,                                                                                  \
                        colour: RED,                                                                    \
                        init: INACTIVE,                                                                 \
                        active: LOW,                                                                    \
                        inactive: HIGH                                                                  \
                },                                                                                                      \
                {       /* LED ID 8     */                                                                      \
                        io_type: IO_EXTENDER_PCF8574AT,                                 \
                        addr:0x20,                                                                              \
                        pin:6,                                                                                  \
                        colour: RED,                                                                    \
                        init: INACTIVE,                                                                 \
                        active: LOW,                                                                    \
                        inactive: HIGH                                                                  \
                },                                                                                                      \
                {       /* LED ID 9             */                                                              \
                        io_type: IO_EXTENDER_PCF8574AT,                                 \
                        addr:0x20,                                                                              \
                        pin:7,                                                                                  \
                        colour: RED,                                                                    \
                        init: INACTIVE,                                                                 \
                        active: LOW,                                                                    \
                        inactive: HIGH                                                                  \
                }                                                                                                       \
        }
#endif /* CONFIG_H_ */