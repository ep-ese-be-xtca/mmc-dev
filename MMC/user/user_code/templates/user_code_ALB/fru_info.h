#ifndef FRU_INFO_H
#define FRU_INFO_H

/*********************************************
 * Common defines
 *********************************************/
#define LANG_CODE						ENGLISH
#define FRU_FILE_ID						"CoreFRU"       //Allows knowing the source of the FRU present in the memory

#define BOARD_INFO_AREA_ENABLE
#define PRODUCT_INFO_AREA_ENABLE
#define MULTIRECORD_AREA_ENABLE

/*********************************************
 * Board information area
 *********************************************/
#define BOARD_MANUFACTURER              "CERN"
#define BOARD_NAME                      "AMC-Loadboard"
#define BOARD_SN                        "000000001"
#define BOARD_PN                        "AMC-L"
 
/*********************************************
 * Product information area
 *********************************************/
#define PRODUCT_MANUFACTURER			"CERN"
#define PRODUCT_NAME                    "AMC LoadBoard"
#define PRODUCT_PN                      "00001"
#define PRODUCT_VERSION                 "v3"
#define PRODUCT_SN                      "00001"
#define PRODUCT_ASSET_TAG               "No tag"
 
/*********************************************
 * AMC: Point to point connectivity record
 *********************************************/
#define PORTCONF_CNT	4

#define PORTCONF_LIST													\
{																	\
	{																\
		port: 4,						/*AMC port*/				\
		protocol: PCIE,					/*Protocol*/				\
		extension: GEN1_NO_SSC,			/*Protocol extension*/		\
		matching: MATCHES_10 			/*Matching*/				\
	},																\
	{																\
		port: 4,						/*AMC port*/				\
		protocol: PCIE,					/*Protocol*/				\
		extension: GEN1_SSC,			/*Protocol extension*/		\
		matching: MATCHES_10 			/*Matching*/				\
	},																\
	{																\
		port: 0,						/*AMC port*/				\
		protocol: ETHERNET,				/*Protocol*/				\
		extension: BASE_1G_BX,			/*Protocol extension*/		\
		matching: MATCHES_10	 		/*Matching*/				\
	},																\
	{																\
		port: 1,						/*AMC port*/				\
		protocol: OEM_TYPE,				/*Protocol*/				\
		OEM: {0x97,0x47,0x06,0xa2,0x2a,0x98,0x48,0xa9,0xbd,0x96,0x7b,0xf3,0x48,0x91,0x36,0x0f},			/*Protocol extension*/		\
		matching: EXACT_MATCHES 		/*Matching*/				\
	}																\
}

/*********************************************
 * AMC: Point to point clock record
 *********************************************/
#define CLOCK_LIST																				\
	{																							\
		{																						\
			clock_id: TCLKA,	/** Defined into the standard */								\
			clock_ctrl: CIPMC,	/** AMC (APP) or Carrier (CIPMC) */								\
			direct_clock_desc_cnt: 1,															\
			direct_clock_desc:																	\
				{																				\
					{																			\
						pll: NO_PLL,	/** Yes (PLL) or No (NO_PLL)*/							\
						type: RECEIVER, /** RECEIVER or SOURCE */								\
						family: UNSPEC_FAMILY, /** Table 3-39 Clock Family definition (AMC.0) */\
						accuracy: 0xFF, /** Depend on the clock family */						\
						freq: KHz(8), /** Clock frequency in Hz */								\
						freq_min: KHz(7), /** Min clock frequency in Hz */						\
						freq_max: KHz(9) /** Max clock frequency in Hz */						\
					}																			\
				}																				\
		}																						\
	}
	
/**********************************************
 * PICMG: Module current record
 **********************************************/
#define MODULE_CURRENT_RECORD            current_in_ma(6500)    //      current_in_ma(max_current_in_mA)
        
#endif