#ifndef CONFIG_H_ 
#define CONFIG_H_ 

/*********************************************
 * PRODUCT INFORMATION 
 *********************************************/
#define IPMI_MSG_MANU_ID_LSB           0x60
#define IPMI_MSG_MANU_ID_B2            0x00
#define IPMI_MSG_MANU_ID_MSB           0x00

#define IPMI_MSG_PROD_ID_LSB           0x35
#define IPMI_MSG_PROD_ID_MSB           0x12

#define MMC_FW_REL_MAJ                 2
#define MMC_FW_REL_MIN                 0

#define FRU_NAME                       "MTF7"

/*********************************************
 * CUSTOM ADDRESSES FOR BENCHTOP 
 *********************************************/
#define CUSTOM_ADDR_LIST   \
    ADDR(0xFF, UNCONNECTED, UNCONNECTED, UNCONNECTED) 

/*********************************************
 * PAYLOD CONFIGURATION 
 ********************************************/

#define POWER_ON_SEQ   \
SET_PAYLOAD_SIGNAL(EN0) \
DELAY(5) \
CLEAR_PAYLOAD_SIGNAL(EN1) \
DELAY(5) \
CLEAR_PAYLOAD_SIGNAL(EN2) \
DELAY(5) \
CLEAR_PAYLOAD_SIGNAL(EN3)

//SET_PAYLOAD_SIGNAL(PWR_GPIO_ENABLE) 

#define POWER_OFF_SEQ   \
SET_PAYLOAD_SIGNAL(EN3) \
DELAY(5) \
SET_PAYLOAD_SIGNAL(EN2) \
DELAY(5) \
SET_PAYLOAD_SIGNAL(EN1) \
DELAY(5) \
CLEAR_PAYLOAD_SIGNAL(EN0)

//CLEAR_PAYLOAD_SIGNAL(PWR_GPIO_ENABLE) 

#endif
