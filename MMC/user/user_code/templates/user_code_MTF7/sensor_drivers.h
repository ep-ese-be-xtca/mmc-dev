/*********************************************
 * Include all the needed headers 
 *********************************************/
#include "../sensors/MTF7_ADC_Voltages.h"
#include "../sensors/MTF7_ADC_Currents.h"
#include "../sensors/MTF7_ADC_Temperatures.h"
