/*
 * ekeying.c
 *
 * Created: 10/04/2015 13:02:51
 *  Author: jumendez
 */ 
#include "../../drivers/drivers.h"

#include "../../application/inc/ekeying.h"
#include "../../application/inc/fru.h"
#include "../../application/inc/sdr.h"

#include "sensors.h"

void ekeying_init(){
	//Point to point connectivity
		set_channel_init(0, ETHERNET, BASE_1G_BX, 0x00);
		set_channel_init(4, PCIE, GEN1_NO_SSC, 0x00);
}

unsigned char port_ekeying_enable(unsigned char port, unsigned char protocol, unsigned char extension){	
	//Return values:
	//	SUCCESS: in case of success
	//	FAILED: in case of error
	//	NI: (not implemented) in case of the state port could not be changed
	
	if(port == 0 && protocol == ETHERNET)	return SUCCESS;
	
	
	return NI;
}

unsigned char port_ekeying_disable(unsigned char port, unsigned char protocol, unsigned char extension){
	//Return values:
	//	SUCCESS: in case of success
	//	FAILED: in case of error
	//	NI: (not implemented) in case of the state port could not be changed
	
	return NI;
}