#ifndef FRU_INFO_H
#define FRU_INFO_H

//#include "../fru.h"
//#include "../languages.h"

/*********************************************
 * Common defines
 *********************************************/
#define LANG_CODE               ENGLISH
#define FRU_FILE_ID             "CoreFRU"       //Allows knowing the source of the FRU present in the memory

#define BOARD_INFO_AREA_ENABLE
#define PRODUCT_INFO_AREA_ENABLE
#define MULTIRECORD_AREA_ENABLE

/*********************************************
 * Board information area
 *********************************************/
#define BOARD_MANUFACTURER              "CERN"
#define BOARD_NAME                      "FC7"
#define BOARD_SN                        "000000000"
#define BOARD_PN                        "FC7-R2"
 
/*********************************************
 * Product information area
 *********************************************/
#define PRODUCT_MANUFACTURER			"CERN"
#define PRODUCT_NAME                    "FC7"
#define PRODUCT_PN                      "R2"
#define PRODUCT_VERSION                 "CERN-v0"
#define PRODUCT_SN                      "00000"
#define PRODUCT_ASSET_TAG               "No tag"
 
/*********************************************
 * AMC: Point to point connectivity record
 *********************************************/
#define PORTCONF_CNT	4

#define PORTCONF_LIST													\
	{																	\
		{																\
			port: 4,						/*AMC port*/				\
			protocol: PCIE,					/*Protocol*/				\
			extension: GEN1_NO_SSC,			/*Protocol extension*/		\
			matching: MATCHES_10 			/*Matching*/				\
		},																\
		{																\
			port: 4,						/*AMC port*/				\
			protocol: PCIE,					/*Protocol*/				\
			extension: GEN1_SSC,			/*Protocol extension*/		\
			matching: MATCHES_10 			/*Matching*/				\
		},																\
		{																\
			port: 0,						/*AMC port*/				\
			protocol: ETHERNET,				/*Protocol*/				\
			extension: BASE_1G_BX,			/*Protocol extension*/		\
			matching: MATCHES_10	 		/*Matching*/				\
		},																\
		{																\
			port: 1,						/*AMC port*/				\
			protocol: OEM_TYPE,				/*Protocol*/				\
			OEM: {0x97,0x47,0x06,0xa2,0x2a,0x98,0x48,0xa9,0xbd,0x96,0x7b,0xf3,0x48,0x91,0x36,0x0f},			/*Protocol extension*/		\
			matching: EXACT_MATCHES 		/*Matching*/				\
		}																\
	}


/*********************************************
 * AMC: Point to point clock record
 *********************************************/
/*	
#define AMC_CLOCK_CONFIGURATION_LIST                                                                                                                                            \
        DIRECT_CLOCK_CONNECTION(TCLKA, CIPMC, NO_PLL, RECEIVER, UNSPEC_FAMILY, 0, KHz(8), KHz(7), KHz(9))               \
        INDIRECT_CLOCK_CONNECTION(USER_CLK1, CIPMC, PLL, RECEIVER, TCLKA)                                                                               \
        DIRECT_CLOCK_CONNECTION(TCLKB, CIPMC, PLL, RECEIVER, UNSPEC_FAMILY, 0, KHz(2), KHz(1), KHz(3))                  \
        DIRECT_CLOCK_CONNECTION(FCLKA, CIPMC, PLL, RECEIVER, UNSPEC_FAMILY, 0, MHz(12.8), MHz(11), MHz(13))
*/

/**********************************************
 * PICMG: Module current record
 **********************************************/
#define MODULE_CURRENT_RECORD            current_in_ma(6500)    //      current_in_ma(max_current_in_mA)
        
#endif