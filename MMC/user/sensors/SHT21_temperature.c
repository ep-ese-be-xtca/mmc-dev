/*
* SHT21_temperature.c
*
* Created: 2015/12/16 18:48:15
*  Author: 
*/

#include <string.h> 
#include <math.h> 

#include "../../drivers/drivers.h" 
#include "../../application/inc/sdr.h" 
#include "../../application/inc/payload.h" 
#include "../../application/inc/project.h" 
#include "../../application/inc/toolfunctions.h" 
#include "../user_code/sensors.h" 

#include "SHT21_temperature.h" 

#ifdef SHT21_temperature
	SHT21_temperature_t SHT21_temperature_sensors[] = SHT21_temperature; 

	#define SHT21_temperature_CNT		(sizeof(SHT21_temperature_sensors) / sizeof((SHT21_temperature_sensors)[0]))

	void SHT21_temperature_init(unsigned char sensI2C){ 
		/** Nothing to be done here */
	}

	unsigned char SHT21_temperature_getvalue(unsigned char sensI2C){ 
		unsigned char user_conf_sht21 = 0x42;
		unsigned char xRes[3];
		unsigned char ack = 0;
		unsigned char timeout = 0;
		unsigned int val = 0;
	
		ext_i2c_send(sensI2C, 0xE7, 1, 1, &user_conf_sht21);	//SHT21 configuration
		ack = ext_i2c_received(sensI2C, 0xF3, 1, 3, xRes);
		while(ack){
			ack = ext_i2c_received(sensI2C, 0, 0, 3, xRes);		//Read value but not send command	
			timeout++;
		}
	
		return xRes[0]; 
	}

	unsigned char update_SHT21_temperature_value(){
		unsigned int i; 
		unsigned char sdr[] = SHT21_temperature_SDR;	

		for(i=0; i < SHT21_temperature_CNT; i++){ 
			/** Check if init */
			if(!SHT21_temperature_sensors[i].is_init){
				if(SHT21_temperature_sensors[i].init_time == MP_PRESENT || (SHT21_temperature_sensors[i].init_time == PP_PRESENT && get_fru_state() == ACTIVATED)){
					if(SHT21_temperature_sensors[i].pre_user_func && SHT21_temperature_sensors[i].pre_user_func(SHT21_temperature_sensors[i].sensor_number)){
						set_sensor_value(SHT21_temperature_sensors[i].sensor_number,0);
						continue;
					}
					SHT21_temperature_init(SHT21_temperature_sensors[i].i2c_addr); 
					if(SHT21_temperature_sensors[i].post_user_func && SHT21_temperature_sensors[i].post_user_func(SHT21_temperature_sensors[i].sensor_number)){
						set_sensor_value(SHT21_temperature_sensors[i].sensor_number,0);
						continue;
					}
					SHT21_temperature_sensors[i].is_init = 1;
				}
			}else{
				if(SHT21_temperature_sensors[i].init_time == PP_PRESENT && get_fru_state() != ACTIVATED){
					SHT21_temperature_sensors[i].is_init = 0;
				}
			}

			/** Call getValue function */
			if(SHT21_temperature_sensors[i].is_init){
				if(SHT21_temperature_sensors[i].pre_user_func && SHT21_temperature_sensors[i].pre_user_func(SHT21_temperature_sensors[i].sensor_number)){
					set_sensor_value(SHT21_temperature_sensors[i].sensor_number,0);
					continue;
				}
				set_sensor_value(SHT21_temperature_sensors[i].sensor_number,SHT21_temperature_getvalue(SHT21_temperature_sensors[i].i2c_addr)); 
				if(SHT21_temperature_sensors[i].post_user_func && SHT21_temperature_sensors[i].post_user_func(SHT21_temperature_sensors[i].sensor_number)){
					set_sensor_value(SHT21_temperature_sensors[i].sensor_number,0);
					continue;
				}
				sensor_state_check(SHT21_temperature_sensors[i].sensor_number, SHT21_temperature_sensors[i].upper_non_rec, SHT21_temperature_sensors[i].upper_critical, SHT21_temperature_sensors[i].upper_non_critical, SHT21_temperature_sensors[i].lower_non_critical, SHT21_temperature_sensors[i].lower_critical, SHT21_temperature_sensors[i].lower_non_rec); 
				check_temp_event(SHT21_temperature_sensors[i].sensor_number, SHT21_temperature_sensors[i].upper_non_rec, SHT21_temperature_sensors[i].upper_critical, SHT21_temperature_sensors[i].upper_non_critical, SHT21_temperature_sensors[i].lower_non_critical, SHT21_temperature_sensors[i].lower_critical, SHT21_temperature_sensors[i].lower_non_rec, sdr[42], sdr[43]); 
			}
			else						set_sensor_value(SHT21_temperature_sensors[i].sensor_number,0); 
		}
		return 0;
	} 

	unsigned char init_SHT21_temperature_sdr(unsigned char sdr_id, unsigned char entity_inst, unsigned char owner_id){
		/** Variables */
		unsigned char i;	
		float x1, x2, y1, y2;	
		float a, b;	
		signed char Bexp, Rexp;	

		unsigned char sdr[] = SHT21_temperature_SDR;	

		/** Computing */
		for(i=0; i < SHT21_temperature_CNT; i++){ 
			SHT21_temperature_sensors[i].id = sdr_id+i;
			SHT21_temperature_sensors[i].entity_inst = entity_inst;
			SHT21_temperature_sensors[i].owner_id = owner_id;

			x1 = SHT21_temperature_sensors[i].p1[0];
			x2 = SHT21_temperature_sensors[i].p2[0];
			y1 =  SHT21_temperature_sensors[i].p1[1];
			y2 =  SHT21_temperature_sensors[i].p2[1];

			a = (y2-y1)/(x2-x1);
			b = y2-(a*x2);

			Bexp = Rexp = 0;

			if(floorf(a) != a || (a > 0 && a > 512) || (a < 1 && a < 512)){
				for(;((a > 0 && a > 512) || (a < 0 && a < -512)) && Rexp < 8; a/=10){
					Rexp++;
					Bexp--;
				}

				for(; ((a > 0 && a < 512) || (a < 0 && a > -512)) && Rexp > -8; a*=10){
					Rexp--;
					Bexp++;
				}

				if((a > 0 && a >= 512) || (a < 0 && a <= -512)){
					a /= 10;
					Rexp++;
					Bexp--;
				}
			}

			if(floorf(b) != b || (b > 0 && b > 512) || (b < 1 && b < 512)){
				for(;((b > 0 && b > 512) || (b < 0 && b < -512)) && Bexp < 8; b/=10){
					Bexp++;
				}

				for(; ((b > 0 && a < 512) || (b < 0 && b > -512)) && Bexp > -8; b*=10){
					Bexp--;
				}

				if((b > 0 && b >= 512) || (b < 0 && b <= -512)){
					b /= 10;
					Bexp++;
				}
			}

			SHT21_temperature_sensors[i].M = floor(a);
			SHT21_temperature_sensors[i].B = floor(b);
			SHT21_temperature_sensors[i].Rexp = Rexp;
			SHT21_temperature_sensors[i].Bexp = Bexp;

			/** Decimal to raw for thresholds */
			SHT21_temperature_sensors[i].upper_non_rec = val_to_raw(SHT21_temperature_sensors[i].upper_non_rec, SHT21_temperature_sensors[i].M, SHT21_temperature_sensors[i].B, SHT21_temperature_sensors[i].Rexp, SHT21_temperature_sensors[i].Bexp, (sdr[20] & 0x80)); 
			SHT21_temperature_sensors[i].upper_critical = val_to_raw(SHT21_temperature_sensors[i].upper_critical, SHT21_temperature_sensors[i].M, SHT21_temperature_sensors[i].B, SHT21_temperature_sensors[i].Rexp, SHT21_temperature_sensors[i].Bexp, (sdr[20] & 0x80)); 
			SHT21_temperature_sensors[i].upper_non_critical = val_to_raw(SHT21_temperature_sensors[i].upper_non_critical, SHT21_temperature_sensors[i].M, SHT21_temperature_sensors[i].B, SHT21_temperature_sensors[i].Rexp, SHT21_temperature_sensors[i].Bexp, (sdr[20] & 0x80)); 
			SHT21_temperature_sensors[i].lower_non_rec = val_to_raw(SHT21_temperature_sensors[i].lower_non_rec, SHT21_temperature_sensors[i].M, SHT21_temperature_sensors[i].B, SHT21_temperature_sensors[i].Rexp, SHT21_temperature_sensors[i].Bexp, (sdr[20] & 0x80)); 
			SHT21_temperature_sensors[i].lower_critical = val_to_raw(SHT21_temperature_sensors[i].lower_critical, SHT21_temperature_sensors[i].M, SHT21_temperature_sensors[i].B, SHT21_temperature_sensors[i].Rexp, SHT21_temperature_sensors[i].Bexp, (sdr[20] & 0x80)); 
			SHT21_temperature_sensors[i].lower_non_critical = val_to_raw(SHT21_temperature_sensors[i].lower_non_critical, SHT21_temperature_sensors[i].M, SHT21_temperature_sensors[i].B, SHT21_temperature_sensors[i].Rexp, SHT21_temperature_sensors[i].Bexp, (sdr[20] & 0x80)); 
		}
		return SHT21_temperature_CNT;
	}

	unsigned char get_SHT21_temperature_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset){
		unsigned int i, j;
		unsigned char sdr[] = SHT21_temperature_SDR;
		unsigned char max_sdr_size = sizeof(sdr);
		unsigned char read_len = 0;	

		for(i=0; i < SHT21_temperature_CNT; i++){
			if(SHT21_temperature_sensors[i].sensor_number == sensor_id){
				for(j=0; j < len && (j+offset) < (max_sdr_size+strlen((const char *)SHT21_temperature_sensors[i].name)); j++, read_len++){
					if(j+offset == 0)						byte[j] = SHT21_temperature_sensors[i].id;
					else if(j+offset == 4)				byte[j] = (max_sdr_size+strlen((const char *)SHT21_temperature_sensors[i].name)) - 5;
					else if(j+offset == 9)				byte[j] = SHT21_temperature_sensors[i].entity_inst;
					else if(j+offset == 5)				byte[j] = SHT21_temperature_sensors[i].owner_id;
					else if(j+offset == 7)				byte[j] = SHT21_temperature_sensors[i].sensor_number;
					else if(j+offset == max_sdr_size-1)	byte[j] = 0xC0 | strlen((const char *)SHT21_temperature_sensors[i].name);
					else if(j+offset == 24)				byte[j] = ((unsigned char)SHT21_temperature_sensors[i].M) & 0xFF;
					else if(j+offset == 25)				byte[j] = (((unsigned char)(SHT21_temperature_sensors[i].M >> 2)) & 0xC0) | (sdr[j+offset] & 0x3F);
					else if(j+offset == 26)				byte[j] = ((unsigned char)SHT21_temperature_sensors[i].B) & 0xFF;
					else if(j+offset == 27)				byte[j] = (((unsigned char)(SHT21_temperature_sensors[i].B >> 2)) & 0xC0) | (sdr[j+offset] & 0x3F);
					else if(j+offset == 29)				byte[j] = ((SHT21_temperature_sensors[i].Rexp & 0x0F) << 4) | (SHT21_temperature_sensors[i].Bexp & 0x0F);
					else if(j+offset == 31)				byte[j] = (SHT21_temperature_sensors[i].upper_non_critical - SHT21_temperature_sensors[i].lower_non_critical)/2;
					else if(j+offset == 32)				byte[j] = SHT21_temperature_sensors[i].upper_non_critical;
					else if(j+offset == 33)				byte[j] = SHT21_temperature_sensors[i].lower_non_critical;
					else if(j+offset == 36)				byte[j] = SHT21_temperature_sensors[i].upper_non_rec;
					else if(j+offset == 37)				byte[j] = SHT21_temperature_sensors[i].upper_critical;
					else if(j+offset == 38)				byte[j] = SHT21_temperature_sensors[i].upper_non_critical;
					else if(j+offset == 39)				byte[j] = SHT21_temperature_sensors[i].lower_non_rec;
					else if(j+offset == 40)				byte[j] = SHT21_temperature_sensors[i].lower_critical;
					else if(j+offset == 41)				byte[j] = SHT21_temperature_sensors[i].lower_non_critical;
					else if(j+offset >= max_sdr_size)		byte[j] = SHT21_temperature_sensors[i].name[(j+offset)-max_sdr_size];
					else									byte[j] = sdr[j+offset];
				}
				return read_len;
			}
		}
		return 0x00;
	}

unsigned char set_SHT21_temperature_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr){ 
	unsigned char i; 

	for(i = 0; i < SHT21_temperature_CNT; i++){ 
		if(SHT21_temperature_sensors[i].sensor_number == sensor_id){ 
			if (mask & 0x01) SHT21_temperature_sensors[i].lower_non_critical = lnc;  
			if (mask & 0x02) SHT21_temperature_sensors[i].lower_critical = lc;  
			if (mask & 0x04) SHT21_temperature_sensors[i].lower_non_rec = lnr;  
			if (mask & 0x08) SHT21_temperature_sensors[i].upper_non_critical = unc; 
			if (mask & 0x10) SHT21_temperature_sensors[i].upper_critical = uc; 
			if (mask & 0x20) SHT21_temperature_sensors[i].upper_non_rec = unr; 

			return 0x00;		 
		} 
	} 

	return 0xFF; 
} 

#endif 
