/*
* MCP9808.c
*
* Created: 2015/12/16 17:55:37
*  Author: 
*/

#include <string.h> 
#include <math.h> 

#include "../../drivers/drivers.h" 
#include "../../application/inc/sdr.h" 
#include "../../application/inc/payload.h" 
#include "../../application/inc/project.h" 
#include "../../application/inc/toolfunctions.h" 
#include "../user_code/sensors.h" 

#include "MCP9808.h" 

#ifdef MCP9808
	MCP9808_t MCP9808_sensors[] = MCP9808; 

	#define MCP9808_CNT		(sizeof(MCP9808_sensors) / sizeof((MCP9808_sensors)[0]))

	void MCP9808_init(unsigned char i2c_addr){
		unsigned char initData = 0x00;
		ext_i2c_send(i2c_addr, 0x08, 1, 1, &initData);
	}

	unsigned char MCP9808_getvalue(unsigned char i2c_addr){
		unsigned char data[2];
		ext_i2c_received(i2c_addr, 0x05, 1, 2, data);
		
		data[0] &= 0x1F;
	
		if((data[0] & 0x10) == 0x10)	return 0;
		else						return (data[0] << 5) + (data[1] >> 3);
	}

	unsigned char update_MCP9808_value(){
		unsigned int i; 
		unsigned char sdr[] = MCP9808_SDR;	

		for(i=0; i < MCP9808_CNT; i++){ 
			/** Check if init */
			if(!MCP9808_sensors[i].is_init){
				if(MCP9808_sensors[i].init_time == MP_PRESENT || (MCP9808_sensors[i].init_time == PP_PRESENT && get_fru_state() == ACTIVATED)){
					if(MCP9808_sensors[i].pre_user_func && MCP9808_sensors[i].pre_user_func(MCP9808_sensors[i].sensor_number)){
						set_sensor_value(MCP9808_sensors[i].sensor_number,0);
						continue;
					}
					MCP9808_init(MCP9808_sensors[i].i2c_addr); 
					if(MCP9808_sensors[i].post_user_func && MCP9808_sensors[i].post_user_func(MCP9808_sensors[i].sensor_number)){
						set_sensor_value(MCP9808_sensors[i].sensor_number,0);
						continue;
					}
					MCP9808_sensors[i].is_init = 1;
				}
			}else{
				if(MCP9808_sensors[i].init_time == PP_PRESENT && get_fru_state() != ACTIVATED){
					MCP9808_sensors[i].is_init = 0;
				}
			}

			/** Call getValue function */
			if(MCP9808_sensors[i].is_init){
				if(MCP9808_sensors[i].pre_user_func && MCP9808_sensors[i].pre_user_func(MCP9808_sensors[i].sensor_number)){
					set_sensor_value(MCP9808_sensors[i].sensor_number,0);
					continue;
				}
				set_sensor_value(MCP9808_sensors[i].sensor_number,MCP9808_getvalue(MCP9808_sensors[i].i2c_addr)); 
				if(MCP9808_sensors[i].post_user_func && MCP9808_sensors[i].post_user_func(MCP9808_sensors[i].sensor_number)){
					set_sensor_value(MCP9808_sensors[i].sensor_number,0);
					continue;
				}
				//sensor_state_check(MCP9808_sensors[i].sensor_number, MCP9808_sensors[i].upper_non_rec, MCP9808_sensors[i].upper_critical, MCP9808_sensors[i].upper_non_critical, MCP9808_sensors[i].lower_non_critical, MCP9808_sensors[i].lower_critical, MCP9808_sensors[i].lower_non_rec); 
				//check_temp_event(MCP9808_sensors[i].sensor_number, MCP9808_sensors[i].upper_non_rec, MCP9808_sensors[i].upper_critical, MCP9808_sensors[i].upper_non_critical, MCP9808_sensors[i].lower_non_critical, MCP9808_sensors[i].lower_critical, MCP9808_sensors[i].lower_non_rec, sdr[42], sdr[43]); 
			}
			else						set_sensor_value(MCP9808_sensors[i].sensor_number,0); 
		}
		return 0;
	} 

	unsigned char init_MCP9808_sdr(unsigned char sdr_id, unsigned char entity_inst, unsigned char owner_id){
		/** Variables */
		unsigned char i;	
		float x1, x2, y1, y2;	
		float a, b;	
		signed char Bexp, Rexp;	

		unsigned char sdr[] = MCP9808_SDR;	

		/** Computing */
		for(i=0; i < MCP9808_CNT; i++){ 
			MCP9808_sensors[i].id = sdr_id+i;
			MCP9808_sensors[i].entity_inst = entity_inst;
			MCP9808_sensors[i].owner_id = owner_id;

			x1 = MCP9808_sensors[i].p1[0];
			x2 = MCP9808_sensors[i].p2[0];
			y1 =  MCP9808_sensors[i].p1[1];
			y2 =  MCP9808_sensors[i].p2[1];

			a = (y2-y1)/(x2-x1);
			b = y2-(a*x2);

			Bexp = Rexp = 0;

			if(floorf(a) != a || (a > 0 && a > 512) || (a < 1 && a < 512)){
				for(;((a > 0 && a > 512) || (a < 0 && a < -512)) && Rexp < 8; a/=10){
					Rexp++;
					Bexp--;
				}

				for(; ((a > 0 && a < 512) || (a < 0 && a > -512)) && Rexp > -8; a*=10){
					Rexp--;
					Bexp++;
				}

				if((a > 0 && a >= 512) || (a < 0 && a <= -512)){
					a /= 10;
					Rexp++;
					Bexp--;
				}
			}

			if(floorf(b) != b || (b > 0 && b > 512) || (b < 1 && b < 512)){
				for(;((b > 0 && b > 512) || (b < 0 && b < -512)) && Bexp < 8; b/=10){
					Bexp++;
				}

				for(; ((b > 0 && a < 512) || (b < 0 && b > -512)) && Bexp > -8; b*=10){
					Bexp--;
				}

				if((b > 0 && b >= 512) || (b < 0 && b <= -512)){
					b /= 10;
					Bexp++;
				}
			}

			MCP9808_sensors[i].M = floor(a);
			MCP9808_sensors[i].B = floor(b);
			MCP9808_sensors[i].Rexp = Rexp;
			MCP9808_sensors[i].Bexp = Bexp;

			/** Decimal to raw for thresholds */
			MCP9808_sensors[i].upper_non_rec = val_to_raw(MCP9808_sensors[i].upper_non_rec, MCP9808_sensors[i].M, MCP9808_sensors[i].B, MCP9808_sensors[i].Rexp, MCP9808_sensors[i].Bexp, (sdr[20] & 0x80)); 
			MCP9808_sensors[i].upper_critical = val_to_raw(MCP9808_sensors[i].upper_critical, MCP9808_sensors[i].M, MCP9808_sensors[i].B, MCP9808_sensors[i].Rexp, MCP9808_sensors[i].Bexp, (sdr[20] & 0x80)); 
			MCP9808_sensors[i].upper_non_critical = val_to_raw(MCP9808_sensors[i].upper_non_critical, MCP9808_sensors[i].M, MCP9808_sensors[i].B, MCP9808_sensors[i].Rexp, MCP9808_sensors[i].Bexp, (sdr[20] & 0x80)); 
			MCP9808_sensors[i].lower_non_rec = val_to_raw(MCP9808_sensors[i].lower_non_rec, MCP9808_sensors[i].M, MCP9808_sensors[i].B, MCP9808_sensors[i].Rexp, MCP9808_sensors[i].Bexp, (sdr[20] & 0x80)); 
			MCP9808_sensors[i].lower_critical = val_to_raw(MCP9808_sensors[i].lower_critical, MCP9808_sensors[i].M, MCP9808_sensors[i].B, MCP9808_sensors[i].Rexp, MCP9808_sensors[i].Bexp, (sdr[20] & 0x80)); 
			MCP9808_sensors[i].lower_non_critical = val_to_raw(MCP9808_sensors[i].lower_non_critical, MCP9808_sensors[i].M, MCP9808_sensors[i].B, MCP9808_sensors[i].Rexp, MCP9808_sensors[i].Bexp, (sdr[20] & 0x80)); 
		}
		return MCP9808_CNT;
	}

	unsigned char get_MCP9808_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset){
		unsigned int i, j;
		unsigned char sdr[] = MCP9808_SDR;
		unsigned char max_sdr_size = sizeof(sdr);
		unsigned char read_len = 0;	

		for(i=0; i < MCP9808_CNT; i++){
			if(MCP9808_sensors[i].sensor_number == sensor_id){
				for(j=0; j < len && (j+offset) < (max_sdr_size+strlen((const char *)MCP9808_sensors[i].name)); j++, read_len++){
					if(j+offset == 0)						byte[j] = MCP9808_sensors[i].id;
					else if(j+offset == 4)				byte[j] = (max_sdr_size+strlen((const char *)MCP9808_sensors[i].name)) - 5;
					else if(j+offset == 8 && MCP9808_sensors[i].entity_inst == 0x60)	byte[j] = 0xC0;
					else if(j+offset == 9)				byte[j] = MCP9808_sensors[i].entity_inst;
					else if(j+offset == 5)				byte[j] = MCP9808_sensors[i].owner_id;
					else if(j+offset == 7)				byte[j] = MCP9808_sensors[i].sensor_number;
					else if(j+offset == max_sdr_size-1)	byte[j] = 0xC0 | strlen((const char *)MCP9808_sensors[i].name);
					else if(j+offset == 24)				byte[j] = ((unsigned char)MCP9808_sensors[i].M) & 0xFF;
					else if(j+offset == 25)				byte[j] = (((unsigned char)(MCP9808_sensors[i].M >> 2)) & 0xC0) | (sdr[j+offset] & 0x3F);
					else if(j+offset == 26)				byte[j] = ((unsigned char)MCP9808_sensors[i].B) & 0xFF;
					else if(j+offset == 27)				byte[j] = (((unsigned char)(MCP9808_sensors[i].B >> 2)) & 0xC0) | (sdr[j+offset] & 0x3F);
					else if(j+offset == 29)				byte[j] = ((MCP9808_sensors[i].Rexp & 0x0F) << 4) | (MCP9808_sensors[i].Bexp & 0x0F);
					else if(j+offset == 31)				byte[j] = (MCP9808_sensors[i].upper_non_critical - MCP9808_sensors[i].lower_non_critical)/2;
					else if(j+offset == 32)				byte[j] = MCP9808_sensors[i].upper_non_critical;
					else if(j+offset == 33)				byte[j] = MCP9808_sensors[i].lower_non_critical;
					else if(j+offset == 36)				byte[j] = MCP9808_sensors[i].upper_non_rec;
					else if(j+offset == 37)				byte[j] = MCP9808_sensors[i].upper_critical;
					else if(j+offset == 38)				byte[j] = MCP9808_sensors[i].upper_non_critical;
					else if(j+offset == 39)				byte[j] = MCP9808_sensors[i].lower_non_rec;
					else if(j+offset == 40)				byte[j] = MCP9808_sensors[i].lower_critical;
					else if(j+offset == 41)				byte[j] = MCP9808_sensors[i].lower_non_critical;
					else if(j+offset >= max_sdr_size)		byte[j] = MCP9808_sensors[i].name[(j+offset)-max_sdr_size];
					else									byte[j] = sdr[j+offset];
				}
				return read_len;
			}
		}
		return 0x00;
	}

unsigned char set_MCP9808_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr){ 
	unsigned char i; 

	for(i = 0; i < MCP9808_CNT; i++){ 
		if(MCP9808_sensors[i].sensor_number == sensor_id){ 
			if (mask & 0x01) MCP9808_sensors[i].lower_non_critical = lnc;  
			if (mask & 0x02) MCP9808_sensors[i].lower_critical = lc;  
			if (mask & 0x04) MCP9808_sensors[i].lower_non_rec = lnr;  
			if (mask & 0x08) MCP9808_sensors[i].upper_non_critical = unc; 
			if (mask & 0x10) MCP9808_sensors[i].upper_critical = uc; 
			if (mask & 0x20) MCP9808_sensors[i].upper_non_rec = unr; 

			return 0x00;		 
		} 
	} 

	return 0xFF; 
} 

#endif 
