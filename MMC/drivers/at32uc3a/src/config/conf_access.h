/*****************************************************************************
 *
 * \file
 *
 * \brief Memory access control configuration file.
 *
 * This file contains the possible external configuration of the memory access
 * control.
 *
 * Copyright (c) 2009-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 ******************************************************************************/
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */


 //! Configuration of ctrl_access which is an abstraction layer for memory interfaces (common/services/storage/ctrl_access)

#ifndef _CONF_ACCESS_H_
#define _CONF_ACCESS_H_

#include "compiler.h"
#include "board.h"

/*! \name Activation of Logical Unit Numbers
 */
//! @{

#define LUN_0	DISABLE
#define LUN_1	DISABLE
#define LUN_2	ENABLE
#define LUN_3	DISABLE
#define LUN_4	DISABLE
#define LUN_5	DISABLE
#define LUN_6	DISABLE
#define LUN_7	DISABLE
#define LUN_USB	DISABLE

//! @}

/*! \name LUN 0 Definitions
 */
//! @{
	
//! @}

/*! \name LUN 1 Definitions
 */
//! @{

//! @}

/*! \name LUN 2 Definitions
 */
//! @{
#define 	LUN_2_INCLUDE				"sd_mmc_mci_mem.h"
#define 	Lun_2_mem_2_ram				sd_mmc_mci_mem_2_ram_0
#define 	LUN_2_NAME					"\"SD/MMC Card over MCI Slot 0\""
#define 	Lun_2_ram_2_mem				sd_mmc_mci_ram_2_mem_0
#define 	Lun_2_read_capacity			sd_mmc_mci_read_capacity_0
#define 	Lun_2_removal				sd_mmc_mci_removal_0
#define 	Lun_2_test_unit_ready		sd_mmc_mci_test_unit_ready_0
#define 	Lun_2_usb_read_10			sd_mmc_mci_usb_read_10_0
#define 	Lun_2_usb_write_10			sd_mmc_mci_usb_write_10_0
#define 	Lun_2_wr_protect			sd_mmc_mci_wr_protect_0
#define 	LUN_ID_SD_MMC_MCI_0_MEM		LUN_ID_2
#define 	SD_MMC_MCI_0_MEM			LUN_2
//! @}

/*! \name LUN 3 Definitions
 */
//! @{

//! @}

/*! \name LUN 4 Definitions
 */
//! @{

//! @}

/*! \name LUN 5 Definitions
 */
//! @{

//! @}

/*! \name USB LUNs Definitions
 */
//! @{

//! @}

/*! \name Actions Associated with Memory Accesses
 *
 * Write here the action to associate with each memory access.
 *
 * \warning Be careful not to waste time in order not to disturb the functions.
 */
//! @{
#define memory_start_read_action(nb_sectors)
#define memory_stop_read_action()
#define memory_start_write_action(nb_sectors)
#define memory_stop_write_action()
//! @}

/*! \name Activation of Interface Features
 */
//! @{

#define ACCESS_USB				DISABLE //!< MEM <-> USB interface.
#define ACCESS_MEM_TO_MEM		ENABLE
#define ACCESS_MEM_TO_RAM		ENABLE
#define ACCESS_STREAM			ENABLE
#define ACCESS_STREAM_RECORD	DISABLE
#define ACCESS_CODEC			DISABLE

//! @}

/*! \name Specific Options for Access Control
 */
//! @{
#define GLOBAL_WR_PROTECT    DISABLE //!< Management of a global write protection.
//! @}

/*! \name Sector size option for different storage media.
 */
//! @{
//#define SECTOR_SIZE  512
//! @}

#endif  // _CONF_ACCESS_H_
