/*
 * addfeatures.h
 *
 * Created: 15/10/2015 10:46:13
 *  Author: jumendez
 */ 


#ifndef ADDFEATURES_H_
#define ADDFEATURES_H_

	/* I2C_ext  defines */
		#define ZERO_LEN	0
		#define BYTE_LEN	1
		#define SHORT_LEN	2
		
	/* ADC feature */
		unsigned char get_8bit_adc(unsigned char ch);


#endif /* ADDFEATURES_H_ */