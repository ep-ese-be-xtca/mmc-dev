/*
 * drivers.h
 *
 * Created: 14/10/2015 14:02:42
 *  Author: jumendez
 */ 


#ifndef DRIVERS_H_
#define DRIVERS_H_	
	
	/* Constants */
	#define MAX_BYTES_READ 20	//I2C buffer size for IPMB messages
	
	/* I2C_ext  defines */
	#define ZERO_LEN	0
	#define BYTE_LEN	1
	#define SHORT_LEN	2
	
	/*Include(s) for I2C extender(s)*/
	#include "i2c_extender/pcf8574.h"
	#include "i2c_extender/pca9698.h"
	
	/* Startup function */
		void boot_uC(void);
		void reset_uC(void);
		
	/* ADC interface */	
		unsigned char payload_voltage_sensor(void);
	
	/* EEPROM interface */
		void write_eeprom_byte(unsigned short addr, unsigned char data);
		unsigned char read_eeprom_byte(unsigned short addr);
	
	/* Startup FLAGs */
		void write_startup_flag(unsigned char flag);
		unsigned char get_startup_flag();
				
	/* Sensor I2C */
		void ext_i2c_send(unsigned char device, unsigned short subAddr, unsigned char nsub, unsigned char length, unsigned char *data);
		unsigned char ext_i2c_received(unsigned char device, unsigned short subAddr, unsigned char nsub, unsigned char length, unsigned char *data);
		unsigned char ext_i2c_poll(unsigned char addr);		
		
	/* IO monitoring and control */
		unsigned char get_signal(unsigned char id);
		void set_signal(unsigned char id);
		void clear_signal(unsigned char id);
		void set_signal_dir(unsigned char id, unsigned char inout);	//Output = 0x00 and Input = 0x01
		
	/* Watchdog feature */
		void watchdog_enable(void);
		void watchdog_disable(void);
		void watchdog_reset(void);
		
	/* Timer */		
		void timer_100ms_attach(void (*userFunc)(void));
		void timer_10ms_attach(void (*userFunc)(void));
		void delay_ms(unsigned short time_ms);		
		void delay_us(unsigned short time_us);
		
	/* IPMB-L bus */
		void ipmb_i2c_configuration(unsigned char address, void (*ipmb_callback)(unsigned char receiveDataLength, unsigned char* recieveData));
		void ipmb_send(unsigned char deviceAddr, unsigned char length, unsigned char* data);
		
	/* Debug */
		void debug(unsigned char *str);
		
	/*Includes - micro-controller dependent*/
	#ifdef ATMEGA128
		#include "atmega128/inc/pinout.h"
		#include "atmega128/inc/addfeatures.h"
	#endif
	
	#ifdef AT32UC3A
		#include <asf.h>
		#include "at32uc3a/inc/pinout.h"
		#include "at32uc3a/inc/addfeatures.h"
	#endif
	
	#ifdef AT32UC3A1512
		#include <asf.h>
		#include "at32uc3a1512/inc/pinout.h"
		//#include "at32uc3a/inc/addfeatures.h"
	#endif

#endif /* DRIVERS_H_ */